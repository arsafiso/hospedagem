<?php namespace october\fin\Models;

use Model;

/**
 * Model
 */
class Caixa extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_fin_caixa';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    function beforeSave(){        
        $old = self::where('id', $this->id)->first();
        
        $atualizaSaldo = 0;
        if(!$old){
            //Criação do caixa
            $atualizaSaldo = 1;
        } else if(!empty($old->saldo) && $old->saldo != $this->saldo){            
            $atualizaSaldo = 1;
        }

        if($atualizaSaldo) {
            $saldo_caixa = SaldoBancario::where('caixa_id', $this->id)->orderBy('id', 'desc')->first();
            if($saldo_caixa && $saldo_caixa->valor_atual != $this->saldo && !empty($this->saldo)){
                $fin = new SaldoBancario;
                $fin->caixa_id = $this->id;
                $fin->data = date('Y-m-d');
                $fin->valor_atual = $this->saldo;
                $fin->pagrec = 0;
                $fin->save();
            }
        }
    }
}
