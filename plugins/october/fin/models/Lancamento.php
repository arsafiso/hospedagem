<?php namespace october\fin\Models;

use Model;
use October\Glo\Models\FichaCadastro;
use October\Glo\Models\Parametro;
use DateTime;
/**
 * Model
 */
class Lancamento extends Model
{
    use \October\Rain\Database\Traits\Validation;    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_fin_lancamento';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'ficha_cadastro' => 'October\Glo\Models\FichaCadastro',
        'ccusto' => CentroCusto::class,
        'formapgto' => FormaPagamento::class,
        'condpgto' => CondicaoPagamento::class,
        'tipodoc' => TipoDocumento::class,
        'caixa' => Caixa::class,
    ];

    public function getFichaCadastroOptions()
    {
        return FichaCadastro::lists('nome', 'id');
    }

    public function getCcustoOptions()
    {
        return CentroCusto::lists('nome', 'id');
    }
    
    public function getFormaPgtoIdOptions()
    {
        return FormaPagamento::lists('nome', 'id');
    }

    public function getCondPgtoIdOptions()
    {
        return CondicaoPagamento::lists('nome', 'id');
    }

    public function getTipoDocIdOptions()
    {
        return TipoDocumento::lists('nome', 'id');
    }
    
    public function getCaixaIdOptions()
    {
        return Caixa::lists('nome', 'id');
    }

    public function beforeSave()
    {
        $parametro = Parametro::getParametro(['caixa_default']);
        $this->caixa_id = (!empty($parametro['caixa_default'])) ? $parametro['caixa_default'] : 0;
        $this->baixaTitulo();
        $this->status = $this->status ?? 1;
    }

    public function afterSave()
    {
        $cond_pgto_avista = CondicaoPagamento::where('condicao_pgto', '0')->lists('id');
        if($cond_pgto_avista[0] != $this->cond_pgto_id && $this->status != 3) {
            $this->geraCondicaoPagamento($cond_pgto_avista[0]);
        } else if ($this->valor_liquido && $this->status == 2) {
            $this->movimentaSaldoBancario();
        }
    }

    public function afterDelete() 
    {
        $saldo = SaldoBancario::where('lancamento_id', $this->id)->first();
        if($saldo){
            $saldo->delete();
        }
    }

    public function geraCondicaoPagamento($cond_pgto_avista) 
    {
        Lancamento::where('tabela_origem', 'fin_lancamento')
        ->where('tabela_origem_id', $this->id)
        ->delete();

        $taxa = FormaPagamento::where('id', $this->forma_pgto_id)->lists('taxa');
        $dt_emissao = $this->data_emissao;
        $condicao = CondicaoPagamento::getCondicaoPagamento($this->cond_pgto_id);
        if(count($condicao) == 1 && strpos($condicao[0], 'x') !== false) {
            $parcelas = intval(str_replace('x', '', $condicao[0]));
            $valor_original = floatval($this->valor_liquido / $parcelas);
            
            if(count($taxa) > 0 && !empty($taxa[0]) && $this->pagrec == 2) {
                $valor_liquido = $valor_original * ((100 - $taxa[0]) / 100);
                $valor_taxa = $valor_original - $valor_liquido;
            } else {
                $valor_liquido = $valor_original;
                $valor_taxa = 0;
            }

            for ($i = 1; $i <= $parcelas; $i++) {
                $vencimento = date("Y-m-d", strtotime('+'.$i.' months', strtotime($dt_emissao)));
                $fin = new Lancamento;
                $fin->data_emissao = $this->data_emissao;
                $fin->data_vencimento = $vencimento;
                $fin->status = 1; //1 = aberto, 2 = pago
                $fin->pagrec = $this->pagrec;
                $fin->numero = $this->numero.'/'.$i;
                $fin->data_emissao = $this->data_emissao;
                $fin->descricao = $this->descricao;
                $fin->caixa_id = $this->caixa_id;
                $fin->ccusto_id = $this->ccusto_id;
                $fin->tipo_doc_id = $this->tipo_doc_id;
                $fin->ficha_cadastro_id = $this->ficha_cadastro_id;
                $fin->forma_pgto_id = $this->forma_pgto_id;
                $fin->cond_pgto_id = $cond_pgto_avista;
                $fin->valor_original = $valor_original;
                $fin->valor_taxas = $valor_taxa;
                $fin->valor_liquido = $valor_liquido;
                $fin->tabela_origem_id = $this->id;
                $fin->tabela_origem = 'fin_lancamento';
                $fin->save();
            }
        } else {
            $valor_original = $this->valor_liquido / count($condicao);
            if(count($taxa) > 0 && !empty($taxa[0]) && $this->pagrec == 2) {
                $valor_liquido = $valor_original * ((100 - $taxa[0]) / 100);
                $valor_taxa = $valor_original - $valor_liquido;
            } else {
                $valor_liquido = $valor_original;
                $valor_taxa = 0;
            }

            $i = 1;
            foreach($condicao as $cond) {
                $vencimento = date("Y-m-d", strtotime('+'.$cond.' day', strtotime($dt_emissao)));
                $fin = new Lancamento;
                $fin->data_emissao = $this->data_emissao;
                $fin->data_vencimento = $vencimento;
                $fin->status = 1; //1 = aberto, 2 = pago
                $fin->pagrec = $this->pagrec;
                $fin->numero = $this->numero.'/'.$i;
                $fin->data_emissao = $this->data_emissao;
                $fin->descricao = $this->descricao;
                $fin->caixa_id = $this->caixa_id;
                $fin->ccusto_id = $this->ccusto_id;
                $fin->tipo_doc_id = $this->tipo_doc_id;
                $fin->ficha_cadastro_id = $this->ficha_cadastro_id;
                $fin->forma_pgto_id = $this->forma_pgto_id;
                $fin->cond_pgto_id = $cond_pgto_avista;
                $fin->valor_original = $valor_original;
                $fin->valor_taxas = $valor_taxa;
                $fin->valor_liquido = $valor_liquido;
                $fin->tabela_origem_id = $this->id;
                $fin->tabela_origem = 'fin_lancamento';
                $fin->save();
                $i++;
            }
        }
    }

    public function baixaTitulo()
    {
        $data_baixa = date('Y-m-d', strtotime($this->data_baixa));
        $data_emissao = date('Y-m-d', strtotime($this->data_emissao));
        if($data_emissao <= $data_baixa && $data_baixa <= date('Y-m-d') && $this->status == 1) {
            $this->status = 2;//1 = aberto, 2 = pago, 3 = Bloqueado
            if($this->tabela_origem == 'fin_lancamento') {
                // Efetuo a baixa do pai também, caso esse seja um título parcelado.
                $lancamento = Self::where('id', $this->tabela_origem_id)->first();
                if($lancamento) {
                    $lancamento->status = 3;//1 = aberto, 2 = pago, 3 = Bloqueado
                    $lancamento->save();
                }
            }
        }
    }

    public function movimentaSaldoBancario() 
    {
        $lancamento = SaldoBancario::where('lancamento_id', $this->id)->first();
            
        $saldo = SaldoBancario::orderBy('id', 'desc')->first();
        if(!$lancamento){
            $fin = new SaldoBancario;
            $fin->lancamento_id = $this->id;
            $fin->caixa_id = $this->caixa_id;
            $fin->valor_movimentado = $this->valor_liquido;
            $fin->data = $this->data_baixa;
            $fin->pagrec = $this->pagrec; 
            $valor_saldo = $saldo->valor_atual ?? 0;
            if($this->pagrec == 1){
                $fin->valor_atual = $valor_saldo - ($this->valor_liquido ?? 0);
            }else{
                $fin->valor_atual = $valor_saldo + ($this->valor_liquido ?? 0);
            }        
            $fin->save();

            $caixa = Caixa::where('id', $this->caixa_id)->first();
            $caixa->saldo = $fin->valor_atual;
            $caixa->save();

        } //Só vai atualizar se o valor tiver sido alterado
        else if ($lancamento->valor_movimentado != $this->valor_liquido){            
            
            //só vai alterar se ainda não houve outra movimentação financeiro posteriormente
            if($lancamento->id == $saldo->id)
            {
                //se a caixa tiver sido alterada
                if($saldo->caixa_id != $this->caixa_id){
                    $saldo_caixa = SaldoBancario::where('caixa_id', $this->caixa_id)->orderBy('id', 'desc')->first();
                    $valor_saldo = $saldo_caixa->valor_atual ?? 0;
                }else{
                    $ultimo_saldo = SaldoBancario::where('caixa_id', $this->caixa_id)->where('id', '<>', $saldo->id)->orderBy('id', 'desc')->first();
                    $valor_saldo = $ultimo_saldo->valor_atual ?? 0;
                }

                $saldo->caixa_id = $this->caixa_id;
                $saldo->valor_movimentado = $this->valor_liquido;
                $saldo->data = $this->data_baixa;
                $saldo->pagrec = $this->pagrec;

                if($this->pagrec == 1){
                    $saldo->valor_atual = $valor_saldo - $this->valor_liquido;
                }else{
                    $saldo->valor_atual = $valor_saldo + $this->valor_liquido;
                }
                $saldo->save();

                $caixa = Caixa::where('id', $this->caixa_id)->first();
                $caixa->saldo = $saldo->valor_atual;
                $caixa->save();
            }
        }
    }
}