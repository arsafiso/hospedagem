<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberFinLancamento3 extends Migration
{
    public function up()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->string('tabela_origem', 30)->nullable();
            $table->renameColumn('movimento_id', 'tabela_origem_id');
        });
    }
    
    public function down()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->dropColumn('tabela_origem');
            $table->renameColumn('tabela_origem_id', 'movimento_id');
        });
    }
}
