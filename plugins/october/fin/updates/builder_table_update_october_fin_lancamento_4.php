<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberFinLancamento4 extends Migration
{
    public function up()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->renameColumn('valor_multa', 'valor_taxas');
        });
    }
    
    public function down()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->renameColumn('valor_taxas', 'valor_multa');
        });
    }
}
