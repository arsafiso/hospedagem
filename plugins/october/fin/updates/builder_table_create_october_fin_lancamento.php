<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberFinLancamento extends Migration
{
    public function up()
    {
        Schema::create('october_fin_lancamento', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable()->unsigned();
            $table->integer('idusuario_alt')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->decimal('valor_original', 10, 2)->nullable();
            $table->decimal('valor_multa', 10, 2)->nullable();
            $table->decimal('valor_juros', 10, 2)->nullable();
            $table->decimal('valor_desconto', 10, 2)->nullable();
            $table->decimal('valor_liquido', 10, 2)->nullable();
            $table->integer('pagrec')->nullable()->unsigned();
            $table->date('data_emissao')->nullable();
            $table->date('data_vencimento')->nullable();
            $table->date('data_baixa')->nullable();
            $table->integer('idficha_cadastro')->nullable()->unsigned();
            $table->integer('idccusto')->nullable()->unsigned();
            $table->integer('idtipodoc')->nullable()->unsigned();
            $table->integer('idcaixa')->nullable()->unsigned();
            $table->integer('idforma_pgto')->nullable()->unsigned();
            $table->integer('idcond_pgto')->nullable()->unsigned();
            $table->integer('status')->nullable()->unsigned();
            $table->string('numero', 30)->nullable();
            $table->text('descricao')->nullable();
            $table->integer('idmovimento')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_fin_lancamento');
    }
}
