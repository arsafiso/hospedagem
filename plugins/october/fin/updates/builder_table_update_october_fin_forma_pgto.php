<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberFinFormaPgto extends Migration
{
    public function up()
    {
        Schema::table('october_fin_forma_pgto', function($table)
        {
            $table->string('abreviacao', 1)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_fin_forma_pgto', function($table)
        {
            $table->dropColumn('abreviacao');
        });
    }
}
