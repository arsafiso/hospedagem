<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberFinCondPgto extends Migration
{
    public function up()
    {
        Schema::table('october_fin_cond_pgto', function($table)
        {
            $table->string('condicao_pgto', 255)->nullable(false)->default('0')->change();
        });
    }
    
    public function down()
    {
        Schema::table('october_fin_cond_pgto', function($table)
        {
            $table->string('condicao_pgto', 255)->nullable()->default(null)->change();
        });
    }
}
