<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberFinFormaPgto2 extends Migration
{
    public function up()
    {
        Schema::table('october_fin_forma_pgto', function($table)
        {
            $table->decimal('taxa', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_fin_forma_pgto', function($table)
        {
            $table->dropColumn('taxa');
        });
    }
}
