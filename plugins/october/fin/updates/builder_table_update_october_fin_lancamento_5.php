<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberFinLancamento5 extends Migration
{
    public function up()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->decimal('valor_original', 10, 2)->default(0)->change();
            $table->decimal('valor_taxas', 10, 2)->default(0)->change();
            $table->decimal('valor_juros', 10, 2)->default(0)->change();
            $table->decimal('valor_desconto', 10, 2)->default(0)->change();
            $table->decimal('valor_liquido', 10, 2)->default(0)->change();
            $table->integer('pagrec')->default(0)->change();
            $table->integer('status')->default(0)->change();
            $table->integer('ficha_cadastro_id')->default(0)->change();
            $table->integer('ccusto_id')->default(0)->change();
            $table->integer('tipo_doc_id')->default(0)->change();
            $table->integer('caixa_id')->default(0)->change();
            $table->integer('forma_pgto_id')->default(0)->change();
            $table->integer('cond_pgto_id')->default(0)->change();
            $table->integer('tabela_origem_id')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('october_fin_lancamento', function($table)
        {
            $table->decimal('valor_original', 10, 2)->default(null)->change();
            $table->decimal('valor_taxas', 10, 2)->default(null)->change();
            $table->decimal('valor_juros', 10, 2)->default(null)->change();
            $table->decimal('valor_desconto', 10, 2)->default(null)->change();
            $table->decimal('valor_liquido', 10, 2)->default(null)->change();
            $table->integer('pagrec')->default(null)->change();
            $table->integer('status')->default(null)->change();
            $table->integer('ficha_cadastro_id')->default(null)->change();
            $table->integer('ccusto_id')->default(null)->change();
            $table->integer('tipo_doc_id')->default(null)->change();
            $table->integer('caixa_id')->default(null)->change();
            $table->integer('forma_pgto_id')->default(null)->change();
            $table->integer('cond_pgto_id')->default(null)->change();
            $table->integer('tabela_origem_id')->default(null)->change();
        });
    }
}
