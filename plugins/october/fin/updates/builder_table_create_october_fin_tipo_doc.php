<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberFinTipoDoc extends Migration
{
    public function up()
    {
        Schema::create('october_fin_tipo_doc', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable()->unsigned();
            $table->integer('idusuario_alt')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('nome', 50)->nullable();
            $table->text('descricao')->nullable();
            $table->integer('pagrec')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_fin_tipo_doc');
    }
}
