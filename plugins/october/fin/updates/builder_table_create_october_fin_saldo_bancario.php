<?php namespace october\fin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberFinSaldoBancario extends Migration
{
    public function up()
    {
        Schema::create('october_fin_saldo_bancario', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable()->unsigned();
            $table->integer('idusuario_alt')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->date('data')->nullable();
            $table->decimal('valor_movimentado', 10, 2)->nullable();
            $table->integer('caixa_id')->nullable()->unsigned();
            $table->integer('lancamento_id')->nullable()->unsigned();
            $table->decimal('valor_atual', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_fin_saldo_bancario');
    }
}
