<?php namespace october\fin;

use DB;
use System\Classes\PluginBase;
use October\Fin\Models\CentroCusto;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }
    
    public function registerListColumnTypes()
    {
        return [
            'status_fin' => [$this, 'statusFinColumn'],
            'status' => [$this, 'statusColumn'],
            'classe' => [$this, 'classeColumn'],
            'centro_pai' => [$this, 'centroPaiColumn'],
            'pagrec' => [$this, 'pagrecColumn'],
        ];
    }

    public function statusFinColumn($value, $column, $record)
    {
        $status = [
            1 => 'Aberto',
            2 => 'Pago',            
        ];

        return array_get($status, $value);
    }

    public function statusColumn($value, $column, $record)
    {
        $status = [
            1 => 'Ativo',
            2 => 'Inativo',            
        ];

        return array_get($status, $value);
    }

    public function classeColumn($value, $column, $record)
    {
        $status = [
            1 => 'Sintético',
            2 => 'Analítico',            
        ];

        return array_get($status, $value);
    }

    public function centroPaiColumn($value, $column, $record)
    {
        if(!empty($value)) {
            $centroCusto = CentroCusto::where('id', $value)->lists('nome');
            return $centroCusto[0];
        }
    }

    public function pagrecColumn($value, $column, $record)
    {
        $status = [
            1 => 'À Pagar',
            2 => 'À Receber',            
        ];

        return array_get($status, $value);
    }
}
