<?php namespace october\fin\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use October\Fin\Models\CondicaoPagamento;

class Lancamentos extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'fin_lancamento' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.fin', 'main-menu-item', 'side-menu-item6');
    }

    public function formBeforeCreate($model)
    {
        $model->pagrec = 1;
    }

    public function formExtendModel($model)
    {
        $model->data_emissao = !empty($model->data_emissao) ? $model->data_emissao : date('Y-m-d');
    }

    public function listExtendQuery($query)
    {
        $cond_pgto_avista = CondicaoPagamento::where('condicao_pgto', '0')->lists('id');
        return $query->where('cond_pgto_id', $cond_pgto_avista[0]);
    }
}
