<?php namespace october\fin\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class SaldoBancarios extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'fin_saldo_bancario' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.fin', 'main-menu-item', 'side-menu-item7');
    }
}
