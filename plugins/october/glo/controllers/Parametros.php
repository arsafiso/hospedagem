<?php namespace october\glo\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Parametros extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'glo_parametro' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.glo', 'main-menu-item', 'side-menu-item3');
    }

    public function listExtendQuery($query)
    {
        $query->where('visivel', 1);
    }
}
