<?php namespace october\glo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Glo\Models\Parametro;
use October\Fin\Models\FormaPagamento;
use October\Fin\Models\CentroCusto;
use October\Fin\Models\Caixa;
use October\Fin\Models\TipoDocumento;
use October\Fin\Models\CondicaoPagamento;
use October\Cfe\Models\TipoMovimento;
use October\Cfe\Models\LocalEstoque;

class BuilderTableCreateCreateDefaultInserts extends Migration
{
    public function up()
    {
        Caixa::updateOrCreate(['id' => 1], [
            'nome' => 'Caixa Interno',
        ]);
        Parametro::updateOrCreate(['nome' => 'caixa_default'], [
            'descricao' => 'Caixa Padrão para as movimentações financeiras',
            'valor' => 1,
            'tabela_origem' => 'october_fin_caixa'
        ]);


        TipoMovimento::updateOrCreate(['id' => 1], [
            'nome' => 'Nota Fiscal',
            'mov_estoque' => 1,
            'mov_financeiro' => 1
        ]);
        Parametro::updateOrCreate(['nome' => 'tipo_mov_nota_fiscal'], [
            'descricao' => 'Tipo de movimento Padrão para Nota Fiscal',
            'valor' => 1,
            'tabela_origem' => 'october_cfe_tipo_movimento'
        ]);


        TipoMovimento::updateOrCreate(['id' => 2], [
            'nome' => 'Movimenta Estoque',
            'mov_estoque' => 1,
            'mov_financeiro' => 0
        ]);
        Parametro::updateOrCreate(['nome' => 'tipo_mov_estoque'], [
            'descricao' => 'Tipo de movimento Padrão para movimentar apenas o estoque',
            'valor' => 2,
            'tabela_origem' => 'october_cfe_tipo_movimento'
        ]);

        
        TipoMovimento::updateOrCreate(['id' => 3], [
            'nome' => 'Movimenta Financeiro',
            'mov_estoque' => 0,
            'mov_financeiro' => 1
        ]);
        Parametro::updateOrCreate(['nome' => 'tipo_mov_financeiro'], [
            'descricao' => 'Tipo de movimento Padrão para movimentar apenas o financeiro',
            'valor' => 3,
            'tabela_origem' => 'october_cfe_tipo_movimento'
        ]);

        
        TipoMovimento::updateOrCreate(['id' => 4], [
            'nome' => 'Movimenta Estoque Cortesia',
            'mov_estoque' => 1,
            'mov_financeiro' => 0
        ]);
        Parametro::updateOrCreate(['nome' => 'tipo_mov_estoque_cortesia'], [
            'descricao' => 'Tipo de movimento Padrão para movimentar apenas o estoque de cortesia',
            'valor' => 4,
            'tabela_origem' => 'october_cfe_tipo_movimento'
        ]);


        LocalEstoque::updateOrCreate(['id' => 1], [
            'nome' => 'Estoque Interno',
        ]);
        Parametro::updateOrCreate(['nome' => 'local_estoque'], [
            'descricao' => 'Local de Estoque Padrão',
            'valor' => 1,
            'tabela_origem' => 'october_cfe_local_estoque'
        ]);


        CondicaoPagamento::updateOrCreate(['id' => 1], [
            'nome' => 'a vista',
            'condicao_pgto' => '0',
        ]);
        Parametro::updateOrCreate(['nome' => 'cond_pgto'], [
            'descricao' => 'Condição de Pagamento Padrão',
            'valor' => 1,
            'tabela_origem' => 'october_fin_cond_pgto'
        ]);

        CondicaoPagamento::updateOrCreate(['id' => 2], [
            'nome' => '30 dias',
            'condicao_pgto' => '30',
        ]);

        CondicaoPagamento::updateOrCreate(['id' => 3], [
            'nome' => '3x',
            'condicao_pgto' => '3x',
        ]);

        Parametro::updateOrCreate(['nome' => 'status_integracao_financeiro'], [
            'descricao' => 'Status do financeiro que um título integrado será atribuído. 1=Aberto, 2=Pago',
            'valor' => 2,
            'visivel' => 1
        ]);

        Parametro::updateOrCreate(['nome' => 'status_integracao_movimento'], [
            'descricao' => 'Status da movimentação de estoque que uma nota integrada será atribuída. 1=Pendente, 2=Integrado',
            'valor' => 2,
            'visivel' => 1
        ]);

        Parametro::updateOrCreate(['nome' => 'valor_cama_adicional'], [
            'descricao' => 'Valor em reais da cama adicional por quarto.',
            'valor' => '50,00',
            'visivel' => 1
        ]);

        Parametro::updateOrCreate(['nome' => 'tempo_tolerancia'], [
            'descricao' => 'Tempo de tolerância para hora excedente, medido em minutos.',
            'valor' => '10',
        ]);

        Parametro::updateOrCreate(['nome' => 'horario_virada_turno'], [
            'descricao' => 'Horário de virada do dia, para valores de estadia.',
            'valor' => '06:00',
        ]);

        Parametro::updateOrCreate(['nome' => 'horario_inicio_pernoite'], [
            'descricao' => 'Horário de início da pernoite, no padrão HH:MM, 24h',
            'valor' => '18:00',
        ]);

        Parametro::updateOrCreate(['nome' => 'horario_termino_pernoite'], [
            'descricao' => 'Horário de término da pernoite, no padrão HH:MM, 24h',
            'valor' => '06:00',
        ]);

        Parametro::updateOrCreate(['nome' => 'valor_pet'], [
            'descricao' => 'Valor em reais que será acrescido por pet, por dia na hospedagem.',
            'valor' => '50,00',
            'visivel' => 1
        ]);

        TipoDocumento::updateOrCreate(['id' => 1], [
            'nome' => 'Nota Fiscal de Entrada',
            'pagrec' => 1,
        ]);

        CentroCusto::updateOrCreate(['id' => 1], [
            'nome' => 'Administrativo',
            'status' => 1,
        ]);

        FormaPagamento::updateOrCreate(['id' => 1], [
            'nome' => 'Dinheiro',
            'abreviacao' => 'D',
        ]);

        FormaPagamento::updateOrCreate(['id' => 2], [
            'nome' => 'Cartão de Crédito',
            'abreviacao' => 'C',
        ]);

        FormaPagamento::updateOrCreate(['id' => 3], [
            'nome' => 'Cartão de Débito',
            'abreviacao' => 'C',
        ]);
    }
    
    public function down()
    {
        Schema::dropIfExists('october_glo_parametros');        
    }
}
