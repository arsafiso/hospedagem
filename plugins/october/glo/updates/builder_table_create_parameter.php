<?php namespace october\glo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Glo\Models\Parametro;

class BuilderTableCreateCreateDefaultInserts extends Migration
{
    public function up()
    {
        Parametro::updateOrCreate(['nome' => 'horario_entrada_saida_reserva'], [
            'descricao' => 'Informe o horário de entrada/saída da reserva.',
            'valor' => '14:00',
            'visivel' => 1
        ]);
    }
    
    public function down()
    {
    }
}
