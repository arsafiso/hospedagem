<?php namespace october\glo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberGloFichaCadastro extends Migration
{
    public function up()
    {
        Schema::table('october_glo_ficha_cadastro', function($table)
        {
            $table->date('dt_nascimento')->nullable();
            $table->string('profissao', 50)->nullable();
            $table->string('nacionalidade', 32)->nullable();
            $table->string('sexo', 1)->nullable();
            $table->string('numero_identidade', 20)->nullable();
            $table->string('tipo_identidade', 30)->nullable();
            $table->string('orgao_expedidor', 10)->nullable();
            $table->string('pais', 32)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_glo_ficha_cadastro', function($table)
        {
            $table->dropColumn('dt_nascimento');
            $table->dropColumn('profissao');
            $table->dropColumn('nacionalidade');
            $table->dropColumn('sexo');
            $table->dropColumn('numero_identidade');
            $table->dropColumn('tipo_identidade');
            $table->dropColumn('orgao_expedidor');
            $table->dropColumn('pais');
        });
    }
}
