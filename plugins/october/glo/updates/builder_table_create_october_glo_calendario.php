<?php namespace october\glo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberGloCalendario extends Migration
{
    public function up()
    {
        Schema::create('october_glo_calendario', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable()->unsigned();
            $table->integer('idusuario_alt')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->date('data')->nullable();
            $table->text('nome')->nullable();
            $table->integer('tipo_data')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_glo_calendario');
    }
}
