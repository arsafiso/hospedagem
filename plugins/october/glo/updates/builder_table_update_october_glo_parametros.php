<?php namespace october\glo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberGloParametros extends Migration
{
    public function up()
    {
        Schema::table('october_glo_parametros', function($table)
        {
            $table->string('tabela_origem', 50)->nullable();
            $table->boolean('visivel')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('october_glo_parametros', function($table)
        {
            $table->dropColumn('tabela_origem');
            $table->dropColumn('visivel');
        });
    }
}
