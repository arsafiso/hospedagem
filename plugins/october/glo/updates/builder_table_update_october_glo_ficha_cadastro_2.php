<?php namespace october\glo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberGloFichaCadastro2 extends Migration
{
    public function up()
    {
        Schema::table('october_glo_ficha_cadastro', function($table)
        {
            $table->boolean('cliente_fornecedor')->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('october_glo_ficha_cadastro', function($table)
        {
            $table->dropColumn('cliente_fornecedor');
        });
    }
}
