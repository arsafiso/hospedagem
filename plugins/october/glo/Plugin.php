<?php namespace october\glo;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerListColumnTypes()
    {
        return [
            'tipo_data' => [$this, 'tipoDataColumn'],
            'percentual' => [$this, 'percentualColumn'],
        ];
    }

    public function tipoDataColumn($value, $column, $record)
    {
        $status = [
            1 => 'Feriado',
            2 => 'Folga',            
        ];

        return array_get($status, $value);
    }

    public function percentualColumn($value, $column, $record)
    {
        return number_format($value, 1, ',', '.').' %';
    }
}
