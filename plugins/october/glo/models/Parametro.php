<?php namespace october\glo\Models;

use Model;

/**
 * Model
 */
class Parametro extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_glo_parametros';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public static function getParametro($parametro) {
        if (is_array($parametro)) {
            $result = Self::whereIn('nome', $parametro)->lists('valor', 'nome');
            $return = [];
            foreach ($result as $nome => $valor) {
                $return[$nome] = $valor;
            }
            if (count($parametro) != count($return)) {
                foreach ($parametro as $valor) {
                    if (!array_key_exists($valor, $return)) {
                        $return[$valor] = '';
                    }
                }
            }
        } else {
            $result = Self::where('nome', $parametro)->lists('valor', 'nome');
            $return = '';
            if (!empty($result)) {
                $return = $result[$parametro];
            }
        }
        return $return;
    }
}
