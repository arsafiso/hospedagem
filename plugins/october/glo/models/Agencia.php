<?php namespace october\glo\Models;

use Model;

/**
 * Model
 */
class Agencia extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_glo_agencia';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
