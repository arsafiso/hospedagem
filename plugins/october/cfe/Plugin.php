<?php namespace october\cfe;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerListColumnTypes()
    {
        return [
            'status_mov' => [$this, 'statusMovColumn'],
            'real' => [$this, 'realColumn'],
            'inteiro' => [$this, 'intColumn'],
        ];
    }

    public function statusMovColumn($value, $column, $record)
    {
        $status = [
            1 => 'Pendente',
            2 => 'Integrado',            
        ];

        return array_get($status, $value);
    }

    public function realColumn($value, $column, $record)
    {
        return 'R$ '.number_format($value, 2, ',', '.');
    }

    public function intColumn($value, $column, $record)
    {
        return number_format($value, 0, ',', '.');
    }
}
