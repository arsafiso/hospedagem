<?php namespace october\cfe\Models;

use Db;
use Model;
use October\Glo\Models\FichaCadastro;
use October\Fin\Models\CentroCusto;
use October\Fin\Models\FormaPagamento;
use October\Fin\Models\CondicaoPagamento;
use October\Fin\Models\TipoDocumento;
use October\Fin\Models\Caixa;
use October\Fin\Models\Lancamento;
use October\Mot\Models\Quarto;
use October\Glo\Models\Parametro;
/**
 * Model
 */
class Movimento extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    public $jsonable = ['produtos'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_cfe_movimento';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'ficha_cadastro' => 'October\Glo\Models\FichaCadastro',
        'ccusto' => CentroCusto::class,
        'forma_pgto' => FormaPagamento::class,
        'cond_pgto' => CondicaoPagamento::class,
        'tipo_doc' => TipoDocumento::class,
        'caixa' => Caixa::class,
        'local_estoque' => LocalEstoque::class,
        'tipo_mov' => TipoMovimento::class,
    ];

    public function getFichaCadastroOptions()
    {
        return FichaCadastro::lists('nome', 'id');
    }

    public function getCcustoOptions()
    {
        return CentroCusto::lists('nome', 'id');
    }
    
    public function getFormaPgtoOptions()
    {
        return FormaPagamento::lists('nome', 'id');
    }

    public function getCondPgtoOptions()
    {
        return CondicaoPagamento::lists('nome', 'id');
    }

    public function getTipodocOptions()
    {
        return TipoDocumento::lists('nome', 'id');
    }
    
    public function getCaixaOptions()
    {
        return Caixa::lists('nome', 'id');
    }

    public function getTipoMovOptions()
    {
        return TipoMovimento::lists('nome', 'id');
    }

    public function getLocalEstoqueOptions()
    {
        return LocalEstoque::lists('nome', 'id');
    }

    public function getProdutosOptions()
    {
        return Produto::lists('nome', 'id');
    }

    public function getQuartoOptions()
    {
        return Quarto::join('october_mot_tipo_quarto', 'october_mot_quarto.tipo_quarto_id', '=', 'october_mot_tipo_quarto.id')
        ->select(DB::raw("CONCAT(october_mot_quarto.numero, ' - ', october_mot_tipo_quarto.nome) as quarto, october_mot_quarto.id as id_quarto"))
        ->where('status', 1)
        ->lists('quarto', 'id_quarto');
    }

    public function beforeSave()
    {
        $status = Lancamento::where('tabela_origem', 'cfe_movimento')->where('tabela_origem_id', $this->id)->lists('status');
        if(count($status) > 0) {
            if(in_array($status[0], [2, 3])) { // 2 - pago, 3 = bloqueado
                return false;
            }
        }
        
        $parametro = Parametro::getParametro(['local_estoque', 'caixa_default']);
        $this->local_estoque_id = (!empty($parametro['local_estoque'])) ? $parametro['local_estoque'] : 0;
        $this->caixa_id = (!empty($parametro['caixa_default'])) ? $parametro['caixa_default'] : 0;
        $this->status = 1;  // 1 - Aberto, 2 - pago, 3 = bloqueado

        if(empty($this->data_emissao)) {
            $this->data_emissao = date('Y-m-d');
        }

        if(!empty($this->produtos) && $this->produtos != '[]'){
            $valor = 0;
            foreach($this->produtos as $prod){
                if(!empty($prod['valor_unit']) && $prod['quantidade'] > 0 && $prod['valor_unit'] > 0)
                {
                    $prod['valor_unit'] = str_replace(',', '.', $prod['valor_unit']);
                    $valor = $valor + (floatval($prod['quantidade']) * floatval($prod['valor_unit']));
                }
            }
            $this->valor = $valor;
        }
    }

    public function afterSave()
    {
        $tipoMov = TipoMovimento::where('id', $this->tipo_mov_id)->first();
        
        if($tipoMov->mov_estoque)
        {
            if($this->produtos != '[]')
            {                
                $itens = [];
                foreach($this->produtos as $prod)
                {
                    if($prod['produtos'] > 0 && $prod['quantidade'] > 0)
                    {
                        $produto = Produto::where('id', $prod['produtos'])->first();
                        if($produto) {
                            if(!empty($produto['produto_composto'])) {
                                foreach($produto['produto_composto'] as $composto) {
                                    $prod_compo['produtos'] = $composto['produto'];
                                    $prod_compo['quantidade'] = ($composto['quantidade'] * $prod['quantidade']);
                                    $prod_compo['valor_unit'] = 0;
                                    $produto = Produto::where('id', $composto['produto'])->first();
                                    $array = $this->configuraProduto($prod_compo, $produto);
                                }
                            } else{
                                $array = $this->configuraProduto($prod, $produto);
                            }
                            $itens[] = $prod['produtos'];
                        }
                    }
                }
                $item = ItemMovimento::where('movimento_id', $this->id)->whereNotIn('produto_id', $itens)->get();
                if($item->count() > 0){
                    foreach($item as $item_prod){
                        $item_prod->delete();
                    }
                }
            }
        }
        
        if($tipoMov->mov_financeiro){
            $fin = Lancamento::where('tabela_origem', 'cfe_movimento')->where('tabela_origem_id', $this->id)->first();
            $taxa = FormaPagamento::where('id', $this->forma_pgto_id)->lists('taxa');
            if(count($taxa) > 0 && !empty($taxa[0]) && $this->pagrec == 2) {
                $valor_liquido = $this->valor * ((100 - $taxa[0]) / 100);
                $valor_taxa = $this->valor - $valor_liquido;
            } else {
                $valor_liquido = $this->valor;
                $valor_taxa = 0;
            }

            if(!$fin){
                $fin = new Lancamento;
                $fin->tabela_origem = 'cfe_movimento';
                $fin->tabela_origem_id = $this->id;
                $fin->status = 1; //1 = aberto, 2 = pago
                $fin->pagrec = $this->pagrec;
            }
            $fin->data_emissao = $this->data_emissao;
            $fin->data_vencimento = $this->data_vencimento;
            $fin->data_baixa = $this->data_baixa;
            $fin->caixa_id = $this->caixa_id;
            $fin->cond_pgto_id = $this->cond_pgto_id;
            $fin->forma_pgto_id = $this->forma_pgto_id;
            $fin->descricao = $this->descricao;
            $fin->ficha_cadastro_id = $this->ficha_cadastro_id;
            $fin->valor_original = $this->valor;
            $fin->valor_taxas = $valor_taxa;
            $fin->valor_liquido = $valor_liquido;
            $fin->save();
        }
    }

    public function afterDelete() {
        
        $balanco = BalancoEstoque::where('movimento_id', $this->id)->get();
        foreach($balanco as $bal){
            $bal->delete();            
        }
        Lancamento::where('tabela_origem', 'cfe_movimento')->where('tabela_origem_id', $this->id)->delete();

        $item = ItemMovimento::where('movimento_id', $this->id)->get();
        if($item->count() > 0){
            foreach($item as $item_prod){
                $item_prod->delete();
            }
        }
    }

    public function configuraProduto($prod, $produto){
        
        $item = ItemMovimento::where('movimento_id', $this->id)->where('produto_id', $prod['produtos'])->first();
        if(!$item){
            $item = new ItemMovimento;
            $item->produto_id = $prod['produtos'];
            $item->movimento_id = $this->id;
        }
        $item->quantidade = $prod['quantidade'];
        $item->valor_unit = $prod['valor_unit'] ?? 0;
        $item->save();        

        $estoque = BalancoEstoque::where('produto_id', $prod['produtos'])
            ->where('local_estoque_id', $this->local_estoque_id)
            ->where('movimento_id', $this->id)->first();
        
        if (!$estoque || $estoque->qtd_movimentada != floatval($prod['quantidade'])) {
            
            $fracao_disponivel = 0;
            if (!empty($produto->fracao)) {
                $fracao_disponivel = ($produto->fracao_disponivel > 0) ? $produto->fracao_disponivel : $produto->fracao;
                $qtd_completa = intval(floatval($prod['quantidade']) / $produto->fracao);
                $resto_fracao = intval(floatval($prod['quantidade']) % $produto->fracao);
                $nova_fracao_disponivel = intval($fracao_disponivel - $resto_fracao);
                if($nova_fracao_disponivel < 0) {
                    $nova_fracao_disponivel = $produto->fracao - ($resto_fracao - $fracao_disponivel);
                    $qtd_completa += 1;
                } else if ($nova_fracao_disponivel == 0) {
                    $nova_fracao_disponivel = $produto->fracao;
                    $qtd_completa += 1;
                }
            }

            if(!$estoque){
                //se nunca teve estoque, vamos pegar a quantidade atual cadastrada.
                $estoque = new BalancoEstoque;
                $estoque->local_estoque_id = $this->local_estoque_id;
                $estoque->movimento_id = $this->id;
                $estoque->produto_id = $prod['produtos'];
                $estoque->pagrec = $this->pagrec;
                
                if($this->pagrec == 1){
                    $estoque_atual = (($produto->qtd_atual ?? 0) + floatval($prod['quantidade']));                
                } else {
                    if(!empty($produto->fracao)){
                        if($qtd_completa > 0){
                            $estoque_atual = (($produto->qtd_atual ?? 0) - $qtd_completa);
                        }else{
                            $estoque_atual = ($produto->qtd_atual ?? 0);
                        }
                    }else{
                        $estoque_atual = (($produto->qtd_atual ?? 0) - floatval($prod['quantidade']));
                    }
                }                                
            }else{
                // Verifico se já houve alguma movimentação desde produto posterior a essa.
                $balanco = BalancoEstoque::where('produto_id', $prod['produtos'])
                ->where('local_estoque_id', $this->local_estoque_id)
                ->where('id', '>', $estoque->id)->first();
                if($balanco) {
                    throw new \ApplicationException('Essa movimentação não poderá ser atualizada, pois o produto "'.$produto->nome.'" já possui movimentações realizadas posteriores a essa, podendo assim prejudicar a integridade do estoque e/ou Balanço Financeiro!');
                    return false;
                }

                // se já teve movimentação de estoque, vamos pegar a quantidade anterior a movimentação atual.
                if($this->pagrec == 1){
                    $estoque_atual = ($estoque->qtd_atual - $estoque->qtd_movimentada) + floatval($prod['quantidade']);
                } else {
                    if(!empty($produto->fracao)){
                        if($qtd_completa > 0){
                            $estoque_atual = ($estoque->qtd_atual - $qtd_completa);
                        }else{
                            $estoque_atual = $estoque->qtd_atual;
                        }
                    }else{
                        $estoque_atual = ($estoque->qtd_atual + $estoque->qtd_movimentada) - floatval($prod['quantidade']);
                    }
                }
            }

            $estoque->qtd_atual = floatval($estoque_atual);
            $estoque->qtd_movimentada = floatval($prod['quantidade']);
            $estoque->save();

            if($fracao_disponivel > 0){
                $produto->qtd_atual = floatval($estoque_atual);
                $produto->fracao_disponivel = $nova_fracao_disponivel;
            } else {
                $produto->qtd_atual = floatval($estoque_atual);
            }
            $produto->save();
        }
    }

    public function filterFields($fields, $context = null) {
        if(!empty($fields->{'tipo_mov'}->value)) {
            $parametros = ['tipo_mov_nota_fiscal', 'tipo_mov_financeiro'];
            $param = Parametro::whereIn('nome', $parametros)->lists('valor', 'nome');
    
            if ($fields->{'tipo_mov'}->value == $param['tipo_mov_financeiro']) {
                $fields->{'produtos'}->hidden = true;
                //$fields->{'quarto'}->hidden = true;
                $fields->{'numero'}->hidden = true;
                $fields->{'valor'}->hidden = false;
                //$fields->{'local_estoque'}->hidden = true;
            } else if($fields->{'tipo_mov'}->value == $param['tipo_mov_nota_fiscal']) {
                //$fields->{'quarto'}->hidden = false;
                $fields->{'numero'}->hidden = true;
                $fields->{'descricao'}->hidden = true;
                $fields->{'produtos'}->hidden = false;
                $fields->{'valor'}->hidden = true;
                // $fields->{'local_estoque'}->hidden = false;
            } else {
                //$fields->{'quarto'}->hidden = true;
                $fields->{'numero'}->hidden = false;
                $fields->{'descricao'}->hidden = false;
                $fields->{'produtos'}->hidden = false;
                $fields->{'valor'}->hidden = true;
                // $fields->{'local_estoque'}->hidden = false;
            }

            /* 
            local_estoque:
            label: 'Local de Estoque'
            nameFrom: name
            descriptionFrom: description
            span: auto
            type: dropdown
            tab: Nota
            dependsOn:
                - tipo_mov

            caixa:
            label: Caixa
            nameFrom: name
            descriptionFrom: description
            span: auto
            type: dropdown
            tab: Financeiro
 
            quarto:
            label: Quarto
            nameFrom: name
            descriptionFrom: description
            span: auto
            type: dropdown
            tab: Nota
            dependsOn:
                - tipo_mov
 
            status:
            label: Status
            options:
                1: Pendente
                2: Integrado
            span: auto
            type: dropdown
            tab: Nota
            */
        }
    }

}