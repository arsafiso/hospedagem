<?php namespace october\cfe\Models;

use Model;
use Mail;
use October\Glo\Models\Parametro;
/**
 * Model
 */
class Produto extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    public $jsonable = ['produto_composto'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_cfe_produto';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function getProdutoOptions()
    {
        return Self::lists('nome', 'id');
    }

    public function afterSave()
    {
        if($this->qtd_atual < $this->qtd_min && !$this->enviou_email) {
            $vars = ['qtd_min' => $this->qtd_min, 'qtd_atual' => $this->qtd_atual, 'produto' => $this->nome];
            /* Mail::send('october.cfe::produto_acabando', $vars, function ($message) {
                $message->to('arsafiso@gmail.com', 'Admin Person');
                $message->subject('Assunto teste');
            });
            $this->enviou_email = 1;
            $this->save(); */
        } elseif ($this->qtd_atual >= $this->qtd_min && $this->enviou_email) {
            $this->enviou_email = 0;
            $this->save();
        }

        $parametro = Parametro::getParametro(['local_estoque']);
        $balanco = BalancoEstoque::where('produto_id', $this->id)
        ->where('local_estoque_id', $parametro['local_estoque'])
        ->orderBy('id','DESC')->first();
        if(!$balanco) {
            $estoque = new BalancoEstoque;
            $estoque->local_estoque_id = $parametro['local_estoque'];
            $estoque->movimento_id = 0;
            $estoque->produto_id = $this->id;
            $estoque->pagrec = 0;
            $estoque->qtd_atual = $this->qtd_atual;
            $estoque->qtd_movimentada = 0;
            $estoque->save();
        } else {
            if ($balanco->qtd_atual != $this->qtd_atual) {
                $estoque = new BalancoEstoque;
                $estoque->local_estoque_id = $parametro['local_estoque'];
                $estoque->movimento_id = 0;
                $estoque->produto_id = $this->id;
                $estoque->pagrec = 0;
                $estoque->qtd_atual = $this->qtd_atual;
                $estoque->qtd_movimentada = 0;
                $estoque->save();
            }
        }
    }
}
