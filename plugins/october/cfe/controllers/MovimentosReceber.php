<?php namespace october\cfe\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class MovimentosReceber extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'cfe_movimento' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.cfe', 'main-menu-item', 'side-menu-item5');
    }

    public function formBeforeCreate($model)
    {
        $model->pagrec = 2;
    }
}
