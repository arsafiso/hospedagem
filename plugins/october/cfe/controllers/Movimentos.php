<?php namespace october\cfe\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Request;
use October\Fin\Models\Lancamento;

class Movimentos extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'cfe_movimento' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.cfe', 'main-menu-item', 'side-menu-item5');
        $this->verificaLancamento();
    }

    public function formBeforeCreate($model)
    {
        $model->pagrec = 1;
    }

    public function verificaLancamento()
    {
        $id = Request::segment(6);
        if($id) {
            $status = Lancamento::where('tabela_origem', 'cfe_movimento')->where('tabela_origem_id', $id)->lists('status');
            if(count($status) > 0) {
                $this->vars['lancamento'] = in_array($status[0], [2, 3]);
            } else {
                $this->vars['lancamento'] = false;
            }
        } else {
            $this->vars['lancamento'] = false;
        }
    }
}
