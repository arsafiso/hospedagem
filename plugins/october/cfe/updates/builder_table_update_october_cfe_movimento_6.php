<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeMovimento6 extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->date('data_baixa')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->dropColumn('data_baixa');
        });
    }
}
