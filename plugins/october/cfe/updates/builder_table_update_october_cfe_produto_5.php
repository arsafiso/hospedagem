<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeProduto5 extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->boolean('enviou_email')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->dropColumn('enviou_email');
        });
    }
}
