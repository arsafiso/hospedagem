<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeMovimento4 extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->integer('tabela_origem_id')->nullable()->unsigned();
            $table->string('tabela_origem', 30)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->dropColumn('tabela_origem_id');
            $table->dropColumn('tabela_origem');
        });
    }
}
