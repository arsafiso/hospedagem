<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeMovimento extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->integer('idtipo_mov')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_movimento', function($table)
        {
            $table->dropColumn('idtipo_mov');
        });
    }
}
