<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeProduto3 extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->integer('fracao')->nullable();
            $table->text('produto_composto')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->dropColumn('fracao');
            $table->dropColumn('produto_composto');
        });
    }
}
