<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeProduto2 extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->decimal('qtd_atual', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->dropColumn('qtd_atual');
        });
    }
}
