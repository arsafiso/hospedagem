<?php namespace october\cfe\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberCfeProduto4 extends Migration
{
    public function up()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->integer('fracao_disponivel')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_cfe_produto', function($table)
        {
            $table->dropColumn('fracao_disponivel');
        });
    }
}
