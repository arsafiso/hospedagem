<?php namespace october\hos\Models;

use Model;
use October\Cfe\Models\Produto;
use October\hos\Models\Temporada;


/**
 * Model
 */
class TipoAcomodacao extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Attribute names to encode and decode using JSON.
     */
    protected $jsonable = ['valor_temporada', 'valor_padrao', 'produtos'];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_hos_tipo_acomodacao';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function getProdutosOptions()
    {
        return Produto::lists('nome', 'id');
    }

    public function getTemporadaOptions()
    {
        return Temporada::lists('nome', 'id');
    }
}
