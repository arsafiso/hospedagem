<?php namespace october\hos\Models;

use Model;

/**
 * Model
 */
class Acomodacao extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Purgeable;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_hos_acomodacao';

    /**
     * @var array List of attributes to purge.
     */
    protected $purgeable = ['qtd_acomodacao'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'nome' => 'required'
    ];

    public $belongsTo = [    
        'tipo_acomodacao' => TipoAcomodacao::class,
    ];

    public function getTipoAcomodacaoOptions()
    {
        return TipoAcomodacao::lists('nome', 'id');
    }

    public function afterSave(){
        /* $qtd_acomodacao = $this->getOriginalPurgeValue('qtd_acomodacao');
        $qtd_acomodacao = $qtd_acomodacao > 0 ? $qtd_acomodacao : 1;
        $acomodacaos = Acomodacao::where('tipo_acomodacao_id', $this->tipo_acomodacao_id)->get()->count();
        $qtd_pendente = $qtd_acomodacao - $acomodacaos;
        if($qtd_pendente > 0){
            for($i = $qtd_pendente; $i > 0; $i--){
                $acomodacao = new Acomodacao;
                $acomodacao->tipo_acomodacao_id = $this->tipo_acomodacao_id;
                $acomodacao->nome = $this->nome;
                $acomodacao->status = $this->status;
                $acomodacao->observacao = $this->observacao;
                $acomodacao->save();
            }
        } */

/*      status:
            label: 'Status da Acomodação'
            span: auto
            type: dropdown
            options:
                - Ocupado
                - Liberado
                - Interditado
                - Arrumação
                - Liberar
        qtd_acomodacao:
            label: 'Quantidade Total desse tipo de Acomodação'
            span: auto
            type: text */
    }
}