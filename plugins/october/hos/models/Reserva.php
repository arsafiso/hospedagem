<?php namespace october\hos\Models;

use Db;
use Model;
use October\Fin\Models\Lancamento;
use October\Fin\Models\FormaPagamento;
use October\Cfe\Models\Produto;
use October\Cfe\Models\Movimento;
use October\Glo\Models\Parametro;
use October\Glo\Models\Agencia;
use October\Glo\Models\FichaCadastro;

/**
 * Model
 */
class Reserva extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Purgeable;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    public $jsonable = ['produto', 'pagamentos'];
    protected $purgeable = ['valor_a_pagar'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_hos_reserva';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'acomodacao' => Acomodacao::class,
        'ficha_cadastro' => FichaCadastro::class,
    ];

    public function getAcomodacaoOptions()
    {   
        return Acomodacao::join('october_hos_tipo_acomodacao', 'october_hos_acomodacao.tipo_acomodacao_id', '=', 'october_hos_tipo_acomodacao.id')
        ->select(DB::raw("CONCAT(october_hos_acomodacao.nome, ' - ', october_hos_tipo_acomodacao.nome) as acomodacao, october_hos_acomodacao.id as id_acomodacao"))
        ->where('status', 1)
        ->lists('acomodacao', 'id_acomodacao');
    }

    public function getProdutoOptions()
    {
        return Produto::lists('nome', 'id');
    }

    public function getFormaPgtoIdOptions()
    {
        return FormaPagamento::lists('nome', 'id');
    }

    public function getFichaCadastroIdOptions()
    {
        return FichaCadastro::lists('nome', 'id');
    }

    public function getAgenciaIdOptions()
    {
        return Agencia::lists('nome', 'id');
    }

    public function beforeSave(){
        
        if(empty($this->agencia_id)) { $this->agencia_id = null; }

        if($this->entrada && $this->saida){
            if(empty($this->id)) {
                if(!$this->verificaDisponibilidade()) {
                    // throw new \ApplicationException('Este quarto já está reservado para essa data!');
                    throw new \ValidationException(['acomodacao_id' => 'Este quarto já está reservado para essa data!']);
                    return false;
                }
            }

            $quarto = Acomodacao::join('october_hos_tipo_acomodacao', 'october_hos_acomodacao.tipo_acomodacao_id', '=', 'october_hos_tipo_acomodacao.id')
            ->select('october_hos_tipo_acomodacao.valor_padrao as valor', 'october_hos_tipo_acomodacao.valor_temporada', 'october_hos_tipo_acomodacao.produtos')
            ->where('october_hos_acomodacao.id', $this->acomodacao_id)->first();

            if ($quarto) {
                $produtos = json_decode($quarto->produtos, true);
                $this->movimentaEstoque($this, $produtos, true);
                $this->valor_estadia = $valor_estadia = $this->calculaValor($quarto);
                if(!empty($this->agencia_id)) {
                    $this->contabilizaAgencia($valor_estadia);
                }
            }
            $json_itens = [];
            $valor_produtos = 0;
            if($this->produto){
                $i = 0;
                foreach($this->produto as $pro){
                    $produto = Produto::where('id', $pro['produto'])->first();
                    $valor_produtos += $produto->preco_venda * ($pro['quantidade'] == '' ? 0 : $pro['quantidade']);
                    $json_itens[$i]['produtos'] = $pro['produto'];
                    $json_itens[$i]['quantidade'] = $pro['quantidade'];
                    $json_itens[$i]['valor_unit'] = $produto->preco_venda;
                    $i++;
                }            
                $this->movimentaEstoque($this, $json_itens, false);
                $this->valor_produtos = $this->getFloat($valor_produtos);
            }

            //PRECISO PREENCHER O CAMPO TOTAL PAGO E TOTAL QUE FALTA SER PAGO
            $this->valor = $this->getFloat($this->valor_produtos ?? 0) + $this->getFloat($this->valor_estadia ?? 0);
            if($this->pagamentos){
                $total_pago = 0;
                foreach($this->pagamentos as $pag){
                    $total_pago += $pag['valor'];
                }
                $this->valor_total_pago = $this->getFloat($total_pago ?? 0);
            }
        }
    }

    public function afterSave()
    {
        $fin = Lancamento::where('tabela_origem', 'hos_reserva')->where('tabela_origem_id', $this->id)->first();
        $taxa = FormaPagamento::where('id', $this->forma_pgto_id)->lists('taxa');
        if(count($taxa) > 0 && !empty($taxa[0])) {
            $valor_liquido = $this->valor * ((100 - $taxa[0]) / 100);
            $valor_taxa = $this->valor - $valor_liquido;
        } else {
            $valor_liquido = $this->valor;
            $valor_taxa = 0;
        }

        if(!$fin){
            $arrayParametro = ['status_integracao_financeiro',
                'caixa_default',
                'cond_pgto'];
            $parametro = Parametro::getParametro($arrayParametro);

            $fin = new Lancamento;
            $fin->tabela_origem = 'hos_reserva';
            $fin->tabela_origem_id = $this->id;
            $fin->data_emissao = $this->created_at;
            $fin->status = $parametro['status_integracao_financeiro'];
            $fin->pagrec = 2;
            $fin->caixa_id = $parametro['caixa_default'];
            $fin->cond_pgto_id = $parametro['cond_pgto'];
            $fin->descricao = 'Títudo originado de Hospedagem';
        }
        $fin->data_vencimento = date("Y-m-d", strtotime($this->saida));
        $fin->data_baixa = date("Y-m-d", strtotime($this->saida));
        $fin->forma_pgto_id = $this->forma_pgto_id;
        $fin->valor_original = $this->valor;
        $fin->valor_taxas = $valor_taxa;
        $fin->valor_liquido = $valor_liquido;
        $fin->save();
    }
    
    public function movimentaEstoque($obj, $json, $default = false){
        if (!$default) {
            $param = Parametro::getParametro(['tipo_mov_estoque']);
            $tipo_mov = $param['tipo_mov_estoque'];
            $descricao = 'Produtos consumidos na Hospedagem.';
            $forma_pgto_id = $obj->forma_pgto_id ?? null;
            $valor_produtos = $obj->valor_produtos ?? 0;
        } else {
            $param = Parametro::getParametro(['tipo_mov_estoque_cortesia']);
            $tipo_mov = $param['tipo_mov_estoque_cortesia'];
            $descricao = 'Produtos Cortesia utilizados na Hospedagem.';
            $forma_pgto_id = null;
            $valor_produtos = 0;
        }

        $mov = Movimento::where('tabela_origem', 'hos_reserva')->where('tabela_origem_id', $obj->id)->where('tipo_mov_id', $tipo_mov)->first();
        
        if(!empty($json) && !empty($obj->id)) {
            if(!$mov){
                $arrayParametro = ['status_integracao_movimento',
                    'local_estoque',
                    'cond_pgto'];
                $parametro = Parametro::getParametro($arrayParametro);

                $mov = new Movimento;
                $mov->tabela_origem = 'hos_reserva';
                $mov->tabela_origem_id = $obj->id;
                $mov->data_emissao = $obj->created_at;
                $mov->data_vencimento = $obj->created_at;
                $mov->status = $parametro['status_integracao_movimento'];
                $mov->pagrec = 2;
                $mov->tipo_mov_id = $tipo_mov;
                $mov->local_estoque_id = $parametro['local_estoque'];
                $mov->cond_pgto_id = $parametro['cond_pgto'];
                $mov->descricao = $descricao;
                $mov->ficha_cadastro_id = $obj->ficha_cadastro_id;
            }
            $mov->forma_pgto_id = $forma_pgto_id;
            $mov->valor = $valor_produtos;
            $mov->produtos = $json;
            $mov->save();
        }else if($mov){
            $mov->delete();
        }
    }

    public function afterDelete() {
        $mov = Movimento::where('tabela_origem', 'hos_reserva')->where('tabela_origem_id', $this->id)->get();
        if($mov->count() > 0){
            foreach($mov as $mov_prod){
                $mov_prod->delete();
            }
        }

        $fin = Lancamento::where('tabela_origem', 'hos_reserva')->where('tabela_origem_id', $this->id)->get();
        if($fin->count() > 0){
            foreach($fin as $financeiro){
                $financeiro->delete();
            }
        }
    }

    public function calculaValor($quarto){
        
        $entrada = date("Y-m-d", strtotime($this->entrada));
        $saida = date("Y-m-d", strtotime($this->saida));
        $diferenca = strtotime($saida) - strtotime($entrada);
        $dias = floor(($diferenca / (60 * 60 * 24)) + 1);
        
        $valor_estadia = $this->calculaValorEstadia($quarto, $entrada, $saida);
        
        if(!empty($this->qtd_camas) && $this->qtd_camas){
            $valor_cama = Parametro::where('nome', 'valor_cama_adicional')->first();
            if ($valor_cama->valor) {
                $valor_cama = $this->getFloat($valor_cama->valor);
                $valor_cama = (($valor_cama * $this->qtd_camas) * $dias);
                $valor_estadia = $valor_estadia + $valor_cama;
                if(!strpos($this->observacao, 'cama adicional')){
                    $observacao = "O valor de R$ ".number_format($valor_cama, 2, ',', '.')." acrescido se refere à cama adicional.\n\n";
                    $this->observacao = $observacao.$this->observacao;
                }
            }
        }

        if(!empty($this->qtd_pet) && $this->qtd_pet){
            $valor_pet = Parametro::where('nome', 'valor_pet')->first();
            if ($valor_pet->valor) {
                $valor_pet = $this->getFloat($valor_pet->valor);
                $valor_pet = (($valor_pet * $this->qtd_pet) * $dias);
                $valor_estadia = $valor_estadia + $valor_pet;
                if(!strpos($this->observacao, 'pet')){
                    $observacao = "O valor de R$ ".number_format($valor_pet, 2, ',', '.')." acrescido se refere ao pet.\n\n";
                    $this->observacao = $observacao.$this->observacao;
                }
            }
        }
        
        $this->valor_estadia_original = $valor_estadia;
        if(!empty($this->valor_desconto)){
            if(strpos($this->valor_desconto, '%')){
                $valor_desconto = str_replace('%', '', $this->valor_desconto);
                $valor_estadia = ((100 - $valor_desconto) / 100) * $valor_estadia;
            }else{
                $valor_estadia -= $this->valor_desconto;
            }
        }
        
        return $valor_estadia ?? 0;
    }

    public function getFloat($str) {
        if(strstr($str, ",")) {
          $str = str_replace(".", "", $str); // replace dots (thousand seps) with blancs
          $str = str_replace(",", ".", $str); // replace ',' with '.'
        }
       
        if(preg_match("#([0-9\.]+)#", $str, $match)) { // search for number that may contain '.'
          return floatval($match[0]);
        } else {
          return floatval($str); // take some last chances with floatval
        }
    }

    public function contabilizaAgencia($valor_estadia) {
        $agencia = Agencia::where('id', $this->agencia_id)->first();
        $this->comissao_agencia = (floatval($agencia->percentual) / 100) * $valor_estadia;
    }

    public function verificaDisponibilidade() {
        $entrada = date("Y-m-d", strtotime($this->entrada));
        $saida = date("Y-m-d", strtotime($this->saida));
        $res = Self::where('acomodacao_id', $this->acomodacao_id)
            ->where(function($query) use ($entrada, $saida) 
            {
                $query->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->where('entrada', '<=', $entrada)->where('saida', '>=', $saida);
                })->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->where('entrada', '>=', $entrada)->where('saida', '<=', $saida);
                })->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->where('entrada', '>=', $entrada)->where('entrada', '<=', $saida);
                })->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->where('saida', '>=', $entrada)->where('saida', '<=', $saida);
                });
            })->get();
        return (count($res) > 0) ? false : true;
    }

    public static function verificaQuartosDisponiveis($entrada, $saida)
    {
        $entrada = date("Y-m-d", strtotime($entrada));
        $saida = date("Y-m-d", strtotime($saida));
        $quartos = Acomodacao::where('status', 1)->get(['nome', 'id', 'tipo_acomodacao_id']);
        $reservas = Reserva::where(function($query) use ($entrada, $saida) 
            {
                $query->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->whereRaw("DATE_FORMAT(entrada, '%Y-%m-%d') <= '".$entrada."'")->whereRaw("DATE_FORMAT(saida, '%Y-%m-%d') >= '".$saida."'");
                })->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->whereRaw("DATE_FORMAT(entrada, '%Y-%m-%d') >= '".$entrada."'")->whereRaw("DATE_FORMAT(saida, '%Y-%m-%d') <= '".$saida."'");
                })->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->whereRaw("DATE_FORMAT(entrada, '%Y-%m-%d') >= '".$entrada."'")->whereRaw("DATE_FORMAT(entrada, '%Y-%m-%d') <= '".$saida."'");
                })->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->whereRaw("DATE_FORMAT(saida, '%Y-%m-%d') >= '".$entrada."'")->whereRaw("DATE_FORMAT(saida, '%Y-%m-%d') <= '".$saida."'");
                })->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->whereRaw("DATE_FORMAT(entrada, '%Y-%m-%d') <= '".$entrada."'")->whereRaw("DATE_FORMAT(saida, '%Y-%m-%d') >= '".$entrada."'");
                })->orWhere(function($query) use ($entrada, $saida) 
                {
                    $query->whereRaw("DATE_FORMAT(entrada, '%Y-%m-%d') <= '".$saida."'")->whereRaw("DATE_FORMAT(saida, '%Y-%m-%d') >= '".$saida."'");
                });
            })->lists('acomodacao_id');

        $disponiveis = [];
        foreach ($quartos as $quarto) {
            if(!in_array($quarto->id, $reservas)) {
                $valor = Self::calculaValorEstadia($quarto->id, $entrada, $saida);
                $disponiveis[] = ['quarto' => $quarto->nome, 'valor' => $valor, 'quarto_id' => $quarto->id, 'tipo_acomodacao_id' => $quarto->tipo_acomodacao_id];
            }
        }
        return $disponiveis;
    }

    public static function calculaValorEstadia($quarto, $entrada, $saida)
    {
        if(!is_object($quarto)) {
            $quarto = Self::buscaQuarto($quarto);
            $entrada = str_replace("/", "-", $entrada);
            $saida = str_replace("/", "-", $saida);

            $entrada = date("Y-m-d", strtotime(trim($entrada)));
            $saida = date("Y-m-d", strtotime(trim($saida)));
        }
        $temporada = Temporada::select('id', 'valor')->where('data_entrada', $entrada)->where('data_saida', $saida)->first();
        $valor_estadia = 0;
        //verifico se existe alguma temporada cadastrada neste periodo
        if (!empty($quarto->valor_temporada) && $temporada) {
            $valor_temporada = json_decode($quarto->valor_temporada);
            foreach ($valor_temporada as $temp) {
                if ($temp->temporada == $temporada->id) {
                    $valor_estadia =  (!empty($temp->valor_periodo)) ? $temp->valor_periodo : $temporada->valor;
                }
            }
        } else if (!empty($quarto->valor)) {
            // verifico se existe algum valor padrão cadastrado para este periodo.
            $intervalo = [];
            while ($entrada <= $saida) {
                $intervalo[] = date('w', strtotime($entrada));
                $entrada = date("Y-m-d", strtotime('+1 day', strtotime($entrada)));
            }
            
            $valor_padrao = json_decode($quarto->valor);
            foreach ($valor_padrao as $padrao) {
                $array_periodo = [];
                foreach($padrao->periodo as $periodo) {
                    $array_periodo[] = $periodo->dia;
                }
                
                if(empty(array_diff_assoc($intervalo , $array_periodo ))) {
                    $valor_estadia = $padrao->valor_periodo;
                    break;
                }
            }
        }

        return $valor_estadia;
    }

    public static function buscaQuarto($acomodacao_id) {
        $quarto = Acomodacao::join('october_hos_tipo_acomodacao', 'october_hos_acomodacao.tipo_acomodacao_id', '=', 'october_hos_tipo_acomodacao.id')
        ->select('october_hos_tipo_acomodacao.valor_padrao as valor', 'october_hos_tipo_acomodacao.valor_temporada', 'october_hos_tipo_acomodacao.produtos')
        ->where('october_hos_acomodacao.id', $acomodacao_id)->first();

        if($quarto) {
            return $quarto;
        } else {
            return false;
        }
    }
}
