<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosReserva2 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->integer('agencia_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->dropColumn('agencia_id');
        });
    }
}
