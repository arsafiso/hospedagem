<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosTipoAcomodacao extends Migration
{
    public function up()
    {
        Schema::table('october_hos_tipo_acomodacao', function($table)
        {
            $table->text('valor_padrao')->nullable();
            $table->renameColumn('valor', 'valor_especifico');
            $table->dropColumn('valor_diaria_padrao');
            $table->dropColumn('valor_diaria_padrao_fds');
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_tipo_acomodacao', function($table)
        {
            $table->dropColumn('valor_padrao');
            $table->renameColumn('valor_especifico', 'valor');
            $table->decimal('valor_diaria_padrao', 10, 2)->nullable();
            $table->decimal('valor_diaria_padrao_fds', 10, 2)->nullable();
        });
    }
}
