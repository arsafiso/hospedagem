<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosReserva9 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->string('ultimo_destino', 64)->nullable();
            $table->string('prox_destino', 64)->nullable();
            $table->string('motivo_viagem', 32)->nullable();
            $table->string('meio_transporte', 32)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->dropColumn('ultimo_destino');
            $table->dropColumn('prox_destino');
            $table->dropColumn('motivo_viagem');
            $table->dropColumn('meio_transporte');
        });
    }
}
