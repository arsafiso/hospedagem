<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosReserva5 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->integer('idusuario_cria')->unsigned()->change();
            $table->integer('idusuario_alt')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->integer('idusuario_cria')->unsigned(false)->change();
            $table->integer('idusuario_alt')->unsigned(false)->change();
        });
    }
}
