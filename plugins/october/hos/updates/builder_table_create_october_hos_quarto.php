<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberHosQuarto extends Migration
{
    public function up()
    {
        Schema::create('october_hos_quarto', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable()->unsigned();
            $table->integer('idusuario_alt')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('tipo_quarto_id')->unsigned();
            $table->integer('status')->unsigned();
            $table->text('observacao')->nullable();
            $table->integer('numero')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_hos_quarto');
    }
}
