<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosReserva7 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->integer('acomodacao_id')->default(0)->change();
            $table->integer('forma_pgto_id')->default(0)->change();
            $table->decimal('valor_produtos', 10, 2)->default(0)->change();
            $table->decimal('valor_estadia', 10, 2)->default(0)->change();
            $table->decimal('valor_desconto', 10, 2)->default(0)->change();
            $table->integer('agencia_id')->default(0)->change();
            $table->decimal('comissao_agencia', 10, 2)->default(0)->change();
            $table->integer('ficha_cadastro_id')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->integer('acomodacao_id')->default(null)->change();
            $table->integer('forma_pgto_id')->default(null)->change();
            $table->decimal('valor_produtos', 10, 2)->default(null)->change();
            $table->decimal('valor_estadia', 10, 2)->default(null)->change();
            $table->decimal('valor_desconto', 10, 2)->default(null)->change();
            $table->integer('agencia_id')->default(null)->change();
            $table->decimal('comissao_agencia', 10, 2)->default(null)->change();
            $table->integer('ficha_cadastro_id')->default(null)->change();
        });
    }
}
