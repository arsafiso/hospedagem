<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosTipoAcomodacao2 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_tipo_acomodacao', function($table)
        {
            $table->text('produtos')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_tipo_acomodacao', function($table)
        {
            $table->dropColumn('produtos');
        });
    }
}
