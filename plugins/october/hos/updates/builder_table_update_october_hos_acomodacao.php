<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosAcomodacao extends Migration
{
    public function up()
    {
        Schema::rename('october_hos_quarto', 'october_hos_acomodacao');
        Schema::table('october_hos_acomodacao', function($table)
        {
            $table->string('nome', 50)->nullable();
            $table->renameColumn('tipo_quarto_id', 'tipo_acomodacao_id');
            $table->dropColumn('numero');
        });
    }
    
    public function down()
    {
        Schema::rename('october_hos_acomodacao', 'october_hos_quarto');
        Schema::table('october_hos_quarto', function($table)
        {
            $table->dropColumn('nome');
            $table->renameColumn('tipo_acomodacao_id', 'tipo_quarto_id');
            $table->integer('numero')->nullable();
        });
    }
}
