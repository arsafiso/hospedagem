<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosReserva8 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->decimal('valor_estadia_original', 10, 2)->nullable()->default(0);
            $table->decimal('valor_total_pago', 10, 2)->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->dropColumn('valor_estadia_original');
            $table->dropColumn('valor_total_pago');
        });
    }
}
