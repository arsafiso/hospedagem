<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosReserva3 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->decimal('comissao_agencia', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->dropColumn('comissao_agencia');
        });
    }
}
