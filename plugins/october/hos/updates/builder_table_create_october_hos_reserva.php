<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberHosReserva extends Migration
{
    public function up()
    {
        Schema::create('october_hos_reserva', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->nullable();
            $table->integer('idusuario_alt')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('acomodacao_id')->unsigned();
            $table->decimal('valor', 10, 2)->nullable();
            $table->dateTime('entrada')->nullable();
            $table->dateTime('saida')->nullable();
            $table->text('produto')->nullable();
            $table->text('observacao')->nullable();
            $table->integer('forma_pgto_id')->nullable()->unsigned();
            $table->decimal('valor_produtos', 10, 2)->nullable();
            $table->decimal('valor_estadia', 10, 2)->nullable();
            $table->decimal('valor_desconto', 10, 2)->nullable();
            $table->string('placa', 10)->nullable();
            $table->string('veiculo', 255)->nullable();
            $table->integer('qtd_camas')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_hos_reserva');
    }
}
