<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosReserva6 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->integer('qtd_pet')->nullable()->unsigned()->default(0);
            $table->integer('qtd_camas')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->dropColumn('qtd_pet');
            $table->integer('qtd_camas')->default(null)->change();
        });
    }
}
