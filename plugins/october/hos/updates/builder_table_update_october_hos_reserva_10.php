<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosReserva10 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->integer('numero_acompanhantes')->nullable()->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_reserva', function($table)
        {
            $table->dropColumn('numero_acompanhantes');
        });
    }
}
