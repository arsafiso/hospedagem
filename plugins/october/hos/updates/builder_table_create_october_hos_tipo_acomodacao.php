<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateOctoberHosTipoAcomodacao extends Migration
{
    public function up()
    {
        Schema::create('october_hos_tipo_acomodacao', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('idempresa')->unsigned()->default(1);
            $table->integer('idfilial')->unsigned()->default(1);
            $table->integer('idusuario_cria')->unsigned();
            $table->integer('idusuario_alt')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('nome', 30)->nullable();
            $table->text('valor')->nullable();
            $table->decimal('valor_diaria_padrao', 10, 2)->nullable();
            $table->decimal('valor_diaria_padrao_fds', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('october_hos_tipo_acomodacao');
    }
}
