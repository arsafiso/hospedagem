<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosTipoAcomodacao4 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_tipo_acomodacao', function($table)
        {
            $table->integer('idusuario_cria')->nullable()->change();
            $table->integer('idusuario_alt')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_tipo_acomodacao', function($table)
        {
            $table->integer('idusuario_cria')->nullable(false)->change();
            $table->integer('idusuario_alt')->nullable(false)->change();
        });
    }
}
