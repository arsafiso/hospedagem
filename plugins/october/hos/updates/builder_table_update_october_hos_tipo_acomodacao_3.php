<?php namespace october\hos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberHosTipoAcomodacao3 extends Migration
{
    public function up()
    {
        Schema::table('october_hos_tipo_acomodacao', function($table)
        {
            $table->renameColumn('valor_especifico', 'valor_temporada');
        });
    }
    
    public function down()
    {
        Schema::table('october_hos_tipo_acomodacao', function($table)
        {
            $table->renameColumn('valor_temporada', 'valor_especifico');
        });
    }
}
