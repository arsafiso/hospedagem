<?php namespace october\hos;

use Db;
use Model;
use System\Classes\PluginBase;
use October\Hos\Models\Acomodacao;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerListColumnTypes()
    {
        return [
            'acomodacao' => [$this, 'acomodacaoColumn'],
            'status_acomodacao' => [$this, 'statusAcomodacaoColumn'],
        ];
    }

    public function acomodacaoColumn($value, $column, $record)
    {
        $acomodacao = Acomodacao::join('october_hos_tipo_acomodacao', 'october_hos_acomodacao.tipo_acomodacao_id', '=', 'october_hos_tipo_acomodacao.id')
        ->select(DB::raw("CONCAT(october_hos_acomodacao.nome, ' - ', october_hos_tipo_acomodacao.nome) as acomodacao, october_hos_acomodacao.id as id_acomodacao"))
        ->where('october_hos_acomodacao.id', $value)
        ->lists('acomodacao');
        return $acomodacao[0];
    }

    public function statusAcomodacaoColumn($value, $column, $record)
    {
        $status = [
            1 => 'Liberado',
            2 => 'Ocupado',
            3 => 'Interditado',
            4 => 'Arrumação',
        ];

        return array_get($status, $value);
    }
}
