<?php namespace october\hos\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class HistoricoReservas extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'hos_reserva' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.hos', 'main-menu-item', 'side-menu-item5');
    }

    public function listExtendQuery($query)
    {
        return $query->whereNotNull('saida')->where('saida', '<', date("Y-m-d H:i:s"));
    }
}
