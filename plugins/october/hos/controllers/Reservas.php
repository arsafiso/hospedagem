<?php namespace october\hos\Controllers;

use Backend\Classes\Controller;
use October\Hos\Models\Reserva;
use October\Glo\Models\FichaCadastro;
use October\Glo\Models\Parametro;
use October\Hos\Models\Acomodacao;
use BackendMenu;
use Carbon\Carbon;

class Reservas extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'hos_reserva' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.hos', 'main-menu-item', 'side-menu-item');
    }

    public function listExtendQuery($query)
    {
        return $query->whereNull('saida')->orWhere('saida', '>=', date("Y-m-d H:i:s"));
    }

    public function formExtendModel($model)
    {
        $model->valor_a_pagar = $model->valor - ($model->valor_total_pago ?? 0);
        $horario = Parametro::getParametro('horario_entrada_saida_reserva') ?? '';
        if (!empty($_POST['data_entrada'])) {
            $model->entrada = Carbon::parse($_POST['data_entrada'] . ' ' . $horario)->format('Y-m-d H:i');
        }
        if (!empty($_POST['data_saida'])) {
            $model->saida = Carbon::parse($_POST['data_saida'] . ' ' . $horario)->format('Y-m-d H:i');
        }
        if (!empty($_POST['radio'])) {
            $model->acomodacao_id = $_POST['radio'];
        }
    }

    public function formAfterSave($model)
    { 
        //criar uma forma de atualizar o formulário ao salvar.
        //return $this->formRenderFieldResult['#Form-field-Controller-date_end-group'] = $this->formRenderField('date_end', ['useContainer' => false]);
    }

    public function Select() {
        if (!empty($_POST)) {
            $this->vars['entrada'] = $_POST['entrada'];
            $this->vars['saida'] = $_POST['saida'];
            
            $data = explode("/", $_POST['entrada']);
            list($dia, $mes, $ano) = $data;
            $entrada = "$ano-$mes-$dia";

            $data = explode("/", $_POST['saida']);
            list($dia, $mes, $ano) = $data;
            $saida = "$ano-$mes-$dia";

            $this->vars['disponiveis'] = Reserva::verificaQuartosDisponiveis($entrada, $saida);
            $this->vars['data_entrada'] = $entrada;
            $this->vars['data_saida'] = $saida;
        } else {
            $this->vars['entrada'] = '';
            $this->vars['saida'] = '';
            $this->vars['disponiveis'] = '';
        }

        return $this->makePartial('select');
    }

    public function Checkin() {
        if (!empty($this->params[0])) {
            $this->vars['sucesso'] = false;
            $reserva = Reserva::select('ficha_cadastro_id', 'acomodacao_id', 'ultimo_destino', 'prox_destino', 'motivo_viagem', 'meio_transporte', 'numero_acompanhantes', 'entrada', 'saida')
                ->where('id', $this->params[0])->first();
            if (!empty($reserva->ficha_cadastro_id)) {
                if ($_POST) {
                    if (!empty($_POST['FichaCadastro']) && !empty($_POST['Reserva'])) {
                        $ficha = FichaCadastro::where('id', $reserva->ficha_cadastro_id)->first();
                        foreach ($_POST['FichaCadastro'] as $key => $value) {
                            if(!empty($value)) {
                                $ficha->$key = $value;
                            }
                        }
                        $ficha->save();

                        foreach ($_POST['Reserva'] as $key => $value) {
                            if(!empty($value)) {
                                $reserva->$key = $value;
                            }
                        }
                        $reserva->save();

                        if ($ficha && $reserva) {
                            $this->vars['sucesso'] = true;
                        }
                    }
                }
                $reserva = Reserva::select('ficha_cadastro_id', 'acomodacao_id', 'ultimo_destino', 'prox_destino', 'motivo_viagem', 'meio_transporte', 'numero_acompanhantes', 'entrada', 'saida')
                ->where('id', $this->params[0])->first();
                $acomodacao = Acomodacao::select('nome')->where('id', $reserva->acomodacao_id)->first();
                $cliente = FichaCadastro::where('id', $reserva->ficha_cadastro_id)->first();
                if (!empty($cliente) && !empty($acomodacao)) {
                    $this->vars['nome'] = $cliente->nome;
                    $this->vars['dt_nascimento'] = $cliente->dt_nascimento;
                    $this->vars['profissao'] = $cliente->profissao;
                    $this->vars['nacionalidade'] = $cliente->nacionalidade;
                    $this->vars['sexo'] = $cliente->sexo;
                    $this->vars['cpf_cnpj'] = $cliente->cpf_cnpj;
                    $this->vars['numero_identidade'] = $cliente->numero_identidade;
                    $this->vars['tipo_identidade'] = $cliente->tipo_identidade;
                    $this->vars['orgao_expedidor'] = $cliente->orgao_expedidor;
                    $this->vars['cep'] = $cliente->cep;
                    $this->vars['rua'] = $cliente->rua;
                    $this->vars['numero'] = $cliente->numero;
                    $this->vars['complemento'] = $cliente->complemento;
                    $this->vars['bairro'] = $cliente->bairro;
                    $this->vars['cidade'] = $cliente->cidade;
                    $this->vars['uf'] = $cliente->uf;
                    $this->vars['pais'] = $cliente->pais;
                    $this->vars['celular'] = $cliente->celular;
                    $this->vars['email'] = $cliente->email;
                    $this->vars['ultimo_destino'] = $reserva->ultimo_destino;
                    $this->vars['prox_destino'] = $reserva->prox_destino;
                    $this->vars['motivo_viagem'] = $reserva->motivo_viagem;
                    $this->vars['meio_transporte'] = $reserva->meio_transporte;
                    $this->vars['acompanhantes'] = $reserva->numero_acompanhantes;
                    $this->vars['entrada'] = date("d/m/Y H:i", strtotime($reserva->entrada));
                    $this->vars['saida'] = date("d/m/Y H:i", strtotime($reserva->saida));
                    $this->vars['acomodacao'] = $acomodacao->nome;
                    $this->vars['meio_transporte_options'] = ['Automóvel', 'Avião', 'Navio', 'Ônibus', 'Trem', 'Outros'];
                    $this->vars['motivo_viagem_options'] = ['Férias', 'Negócios', 'Congresso', 'Estudos', 'Saúde', 'Outros'];
                    $this->vars['estados'] = [
                        [0 => 'AC', 1 => 'Acre'],
                        [0 => 'AL', 1 => 'Alagoas'],
                        [0 => 'AP', 1 => 'Amapá'],
                        [0 => 'AM', 1 => 'Amazonas'],
                        [0 => 'BA', 1 => 'Bahia'],
                        [0 => 'CE', 1 => 'Ceará'],
                        [0 => 'DF', 1 => 'Distrito Federal'],
                        [0 => 'ES', 1 => 'Espírito Santo'],
                        [0 => 'GO', 1 => 'Goiás'],
                        [0 => 'MA', 1 => 'Maranhão'],
                        [0 => 'MT', 1 => 'Mato Grosso'],
                        [0 => 'MS', 1 => 'Mato Grosso do Sul'],
                        [0 => 'MG', 1 => 'Minas Gerais'],
                        [0 => 'PA', 1 => 'Pará'],
                        [0 => 'PB', 1 => 'Paraíba'],
                        [0 => 'PR', 1 => 'Paraná'],
                        [0 => 'PE', 1 => 'Pernambuco'],
                        [0 => 'PI', 1 => 'Piauí'],
                        [0 => 'RJ', 1 => 'Rio de Janeiro'],
                        [0 => 'RN', 1 => 'Rio Grande do Norte'],
                        [0 => 'RS', 1 => 'Rio Grande do Sul'],
                        [0 => 'RO', 1 => 'Rondônia'],
                        [0 => 'RR', 1 => 'Roraima'],
                        [0 => 'SC', 1 => 'Santa Catarina'],
                        [0 => 'SP', 1 => 'São Paulo'],
                        [0 => 'SE', 1 => 'Sergipe'],
                        [0 => 'TO', 1 => 'Tocantins']];
                }
            }
            
        }
        return $this->makePartial('checkin');
    }
}
