<?php namespace october\hos\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Temporadas extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'hos_temporada' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.hos', 'main-menu-item', 'side-menu-item4');
    }
}
