<?php namespace october\mot\Models;

use Model;

/**
 * Model
 */
class Quarto extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Purgeable;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array List of attributes to purge.
     */
    protected $purgeable = ['qtd_quarto'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_mot_quarto';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'numero' => 'required'
    ];

    public $belongsTo = [    
        'tipo_quarto' => TipoQuarto::class,
    ];

    public function getTipoQuartoOptions()
    {
        return TipoQuarto::lists('nome', 'id');
    }

    public function afterSave(){
        $qtd_quarto = $this->getOriginalPurgeValue('qtd_quarto');
        $qtd_quarto = $qtd_quarto > 0 ? $qtd_quarto : 1;

        $quartos = Quarto::where('tipo_quarto_id', $this->tipo_quarto_id)->get()->count();

        $qtd_pendente = $qtd_quarto - $quartos;
        
        if($qtd_pendente > 0){
            for($i = $qtd_pendente; $i > 0; $i--){
                $quarto = new Quarto;
                $quarto->tipo_quarto_id = $this->tipo_quarto_id;
                $quarto->status = 1;
                $quarto->observacao = $this->observacao;
                $quarto->save();
            }
        }
    }
}