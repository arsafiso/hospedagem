<?php namespace october\mot\Models;

use Model;

/**
 * Model
 */
class TipoQuarto extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    
    /**
     * @var array Attribute names to encode and decode using JSON.
     */
    protected $jsonable = ['valor'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_mot_tipo_quarto';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
