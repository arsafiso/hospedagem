<?php namespace october\mot\Models;

use Db;
use Model;
use Flash;
use October\Fin\Models\Lancamento;
use October\Fin\Models\FormaPagamento;
use October\Cfe\Models\Produto;
use October\Cfe\Models\Movimento;
use October\Glo\Models\Calendario;
use October\Glo\Models\Parametro;

/**
 * Model
 */
class Reserva extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Purgeable;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    public $jsonable = ['produto'];
    protected $purgeable = ['valor_estadia_original'];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'october_mot_reserva';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'quarto' => Quarto::class,
    ];
    
    public function beforeSave(){
        
        $tempo = $this->consultaParametros('tempo_tolerancia');
        if(!empty($this->saida) && date("Y-m-d H:i") > date("Y-m-d H:i", strtotime('+ '.$tempo.' minutes',strtotime($this->saida))) ){
            Flash::success('Não é possível editar reservas encerradas há mais de '.$tempo.' minutos!');
            return false;
        }
        
        if($this->entrada && $this->saida){
            $this->valor_estadia = $this->calculaValor();

            $json_itens = [];
            $valor_produtos = 0;
            if($this->produto){
                $i = 0;
                foreach($this->produto as $pro){
                    $produto = Produto::where('id', $pro['produto'])->first();
                    $valor_produtos += $produto->preco_venda * ($pro['quantidade'] == '' ? 0 : $pro['quantidade']);
                    $json_itens[$i]['produtos'] = $pro['produto'];
                    $json_itens[$i]['quantidade'] = $pro['quantidade'];
                    $json_itens[$i]['valor_unit'] = $produto->preco_venda;
                    $i++;
                }            
            }
            $this->valor_produtos = $valor_produtos;
            $this->movimentaEstoque($this, $json_itens);

            $this->valor = intval($this->valor_produtos ?? 0) + intval($this->valor_estadia ?? 0);
        }
    }

    public function afterSave()
    {
        $fin = Lancamento::where('tabela_origem', 'mot_reserva')->where('tabela_origem_id', $this->id)->first();
        $taxa = FormaPagamento::where('id', $this->forma_pgto_id)->lists('taxa');
        if(count($taxa) > 0 && !empty($taxa[0])) {
            $valor_liquido = $this->valor * ((100 - $taxa[0]) / 100);
            $valor_taxa = $this->valor - $valor_liquido;
        } else {
            $valor_liquido = $this->valor;
            $valor_taxa = 0;
        }

        if(!$fin){
            $fin = new Lancamento;
            $fin->tabela_origem = 'mot_reserva';
            $fin->tabela_origem_id = $this->id;
            $fin->data_emissao = $this->created_at;
            $fin->data_vencimento = $this->created_at;
            $fin->data_baixa = $this->created_at;
            $fin->status = 2;
            $fin->pagrec = 2;
            $fin->caixa_id = 1;
            $fin->cond_pgto_id = 1;
            $fin->forma_pgto_id = $this->forma_pgto_id;
            $fin->descricao = 'Títudo originado de Estadia';
        }
        $fin->valor_original = $this->valor;
        $fin->valor_taxas = $valor_taxa;
        $fin->valor_liquido = $valor_liquido;
        $fin->save();
    }

    public function getQuartoOptions()
    {   
        return Quarto::join('october_mot_tipo_quarto', 'october_mot_quarto.tipo_quarto_id', '=', 'october_mot_tipo_quarto.id')
        ->select(DB::raw("CONCAT(october_mot_quarto.numero, ' - ', october_mot_tipo_quarto.nome) as quarto, october_mot_quarto.id as id_quarto"))
        ->where('status', 1)
        ->lists('quarto', 'id_quarto');
    }

    public function getProdutoOptions()
    {
        return Produto::lists('nome', 'id');
    }

    public function getFormaPgtoIdOptions()
    {
        return FormaPagamento::lists('nome', 'id');
    }
    
    public function time2seconds($time='00:00:00')
    {
        list($hours, $mins, $secs) = explode(':', $time);
        return ($hours * 3600 ) + ($mins * 60 ) + $secs;
    }

    public function movimentaEstoque($obj, $json){
        
        $mov = Movimento::where('tabela_origem', 'mot_reserva')->where('tabela_origem_id', $obj->id)->first();
        
        if(!empty($json)){
            if(!$mov){
                $arrayParametro = ['tipo_mov_estoque', 
                    'status_integracao_movimento',
                    'local_estoque',
                    'cond_pgto'];
                $parametro = Parametro::getParametro($arrayParametro);

                $mov = new Movimento;
                $mov->tabela_origem = 'mot_reserva';
                $mov->tabela_origem_id = $obj->id;
                $mov->data_emissao = $obj->created_at;
                $mov->data_vencimento = $obj->created_at;
                $mov->status = $parametro['status_integracao_movimento'];
                $mov->pagrec = 2;
                $mov->tipo_mov_id = $parametro['tipo_mov_estoque'];
                $mov->local_estoque_id = $parametro['local_estoque'];
                $mov->cond_pgto_id = $parametro['cond_pgto'];
                $mov->descricao = 'Produtos consumidos na estadia.';
            }
            $mov->forma_pgto_id = $obj->forma_pgto_id;
            $mov->valor = $obj->valor_produtos;
            $mov->produtos = $json;
            $mov->save();
        }else if($mov){
            $mov->delete();
        }
    }

    public function afterDelete() {
        $mov = Movimento::where('tabela_origem', 'mot_reserva')->where('tabela_origem_id', $this->id)->get();
        if($mov->count() > 0){
            foreach($mov as $mov_prod){
                $mov_prod->delete();
            }
        }
    }

    public function calculaTempo($parametros){
               
        if(strtotime(date('H:i')) >= strtotime($parametros['turno']) && strtotime($parametros['turno']) >= strtotime('00:00')){
            $dia_entrada_cobrada = date("Y-m-d", strtotime($this->entrada));
        }else{
            $dia_entrada_cobrada = date("Y-m-d", strtotime('-1 day', $this->entrada));
        }

        $entrada = date("Y-m-d", strtotime($this->entrada));
        $calendario = Calendario::where('data', $entrada)->where('tipo_data', 1)->first();
        
        if($calendario){
            $tempo['dia'] = 7; //dia referente a feriado
            $tempo['dia_entrada_cobrada'] = 7;            
        }else{
            $tempo['dia'] = date('w', strtotime($entrada));
            $tempo['dia_entrada_cobrada'] = date('w', strtotime($dia_entrada_cobrada));            
        }
        
        $tempo['tempo'] = ((strtotime($this->saida ?? date('Y-m-d H:i:s')) - strtotime($this->entrada)) / 60);        

        return $tempo;
    }

    public function calculaValor(){
        
        $parametros = $this->consultaParametros();

        $retorno = $this->calculaTempo($parametros);

        $tempo = $retorno['tempo'];
        $dia = $retorno['dia'];
        $dia_entrada_cobrada = $retorno['dia_entrada_cobrada'];
        $inicio_pernoite = $parametros['inicio_pernoite'];
        $termino_pernoite = $parametros['termino_pernoite'];

        $quarto = Quarto::join('october_mot_tipo_quarto', 'october_mot_quarto.tipo_quarto_id', '=', 'october_mot_tipo_quarto.id')
        ->select('october_mot_tipo_quarto.valor', 'valor_hora_padrao', 'valor_2horas_padrao', 'valor_pernoite_padrao', 'valor_perdia_padrao', 'valor_hora_avulsa_padrao')
        ->where('october_mot_quarto.id', $this->quarto_id)->first();

        if($quarto){
            if(strtotime($this->entrada) >= strtotime($inicio_pernoite) && strtotime($this->entrada) < strtotime($termino_pernoite)){
                $pernoite_dia = 'valor_pernoite';
                $permanencia_padrao = $quarto->valor_pernoite_padrao;
            }else{
                $pernoite_dia = 'valor_perdia';
                $permanencia_padrao = $quarto->valor_perdia_padrao;
            }

            if($quarto->valor){
                $valor = json_decode($quarto->valor);

                foreach($valor as $array){                
                    $array = json_decode(json_encode($array), True);
                    //verifica se existe preço cadastrado para este dia da semana e horário específico
                    if($array['dia'] == $dia && date('H:i', strtotime($array['hora_inicio'])) <= date('H:i', strtotime($this->entrada)) && date('H:i', strtotime($array['hora_fim'])) >= date('H:i', strtotime($this->entrada))){
                        
                        $valor_estadia = $this->calculaValorPorTempo($tempo, $array['valor_hora'], $array['valor_2_horas'], $array['valor_hora_avulsa'], $array[$pernoite_dia]);
                        break;
                    }else if($array['dia'] == $dia_entrada_cobrada && empty($array['hora_inicio']) && empty($array['hora_fim'])){
                        
                        $valor_estadia = $this->calculaValorPorTempo($tempo, $array['valor_hora'], $array['valor_2_horas'], $array['valor_hora_avulsa'], $array[$pernoite_dia]);
                        break;
                    }else{
                        
                        $vlr_hora = $quarto->valor_hora_padrao;
                        $vlr_2h = $quarto->valor_2horas_padrao;
                        $vlr_avulsa = $quarto->valor_hora_avulsa_padrao;
                        $vlr_pernoite = $permanencia_padrao;
                        
                        $valor_estadia = $this->calculaValorPorTempo($tempo, $vlr_hora, $vlr_2h, $vlr_avulsa, $vlr_pernoite);
                    }
                }
            }
            
            if(!empty($this->qtd_pessoas) && $this->qtd_pessoas > 2){
                $valor_estadia = (($valor_estadia / 2) * $this->qtd_pessoas);
            }
            
            /* $this->valor_estadia_original = $valor_estadia; */

            if(!empty($this->valor_desconto)){
                if(strpos($this->valor_desconto, '%')){
                    $valor_desconto = str_replace('%', '', $this->valor_desconto);
                    $valor_estadia = ((100 - $valor_desconto) / 100) * $valor_estadia;
                }else{
                    $valor_estadia -= $this->valor_desconto;
                }
            }

        }else{
            $valor_estadia = 0;
        } 

        return $valor_estadia;
    }

    public function consultaParametros($parametros = null){
        
        if($parametros){
            $horario = Parametro::where('nome', $parametros)->first();
            $parametro = $horario->valor;
        }else{
            $parametros = ['horario_virada_turno', 'horario_inicio_pernoite', 'horario_termino_pernoite'];
            $horario = Parametro::whereIn('nome', $parametros)->get();

            foreach($horario as $valor){
                if($valor->nome == 'horario_virada_turno'){
                    $parametro['turno'] = $valor->valor;
                } else if($valor->nome == 'horario_inicio_pernoite'){
                    $parametro['inicio_pernoite'] = $valor->valor;
                } else if($valor->nome == 'horario_termino_pernoite'){
                    $parametro['termino_pernoite'] = $valor->valor;
                }
            }
        }

        return $parametro;
    }

    public function calculaValorPorTempo($tempo, $vlr_hora, $vlr_2h, $vlr_avulsa, $vlr_pernoite){
        if($tempo <= 60) {
            $valor_estadia =  $vlr_hora;
        }else if($tempo > 60 && $tempo <= 120) {
            $valor_estadia =  $vlr_2h;
        }else if($tempo > 120 && $tempo <= 180) {
            $hora_avulsa = ($vlr_2h + $vlr_avulsa);
            if($vlr_pernoite > $hora_avulsa){
                $valor_estadia = $hora_avulsa;
            }else{
                $valor_estadia = $vlr_pernoite;
            }
        }else{
            $valor_estadia = $vlr_pernoite;
        }  

        return $valor_estadia;
    }
}