<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotQuarto4 extends Migration
{
    public function up()
    {
        Schema::table('october_mot_quarto', function($table)
        {
            $table->integer('numero')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_quarto', function($table)
        {
            $table->dropColumn('numero');
        });
    }
}
