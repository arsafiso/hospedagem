<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotQuarto extends Migration
{
    public function up()
    {
        Schema::table('october_mot_quarto', function($table)
        {
            $table->dateTime('entrada')->nullable();
            $table->dateTime('saida')->nullable();
            $table->integer('idtipo_quarto')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_quarto', function($table)
        {
            $table->dropColumn('entrada');
            $table->dropColumn('saida');
            $table->dropColumn('idtipo_quarto');
        });
    }
}
