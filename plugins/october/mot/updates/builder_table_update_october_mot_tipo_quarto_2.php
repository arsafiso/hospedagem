<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotTipoQuarto2 extends Migration
{
    public function up()
    {
        Schema::table('october_mot_tipo_quarto', function($table)
        {
            $table->decimal('valor_2horas_padrao', 10, 2)->nullable();
            $table->decimal('valor_pernoite_padrao', 10, 2)->nullable();
            $table->renameColumn('valor_padrao', 'valor_hora_padrao');
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_tipo_quarto', function($table)
        {
            $table->dropColumn('valor_2horas_padrao');
            $table->dropColumn('valor_pernoite_padrao');
            $table->renameColumn('valor_hora_padrao', 'valor_padrao');
        });
    }
}
