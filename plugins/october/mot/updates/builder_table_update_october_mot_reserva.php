<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotReserva extends Migration
{
    public function up()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->text('observacao')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->dropColumn('observacao');
        });
    }
}
