<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotTipoQuarto3 extends Migration
{
    public function up()
    {
        Schema::table('october_mot_tipo_quarto', function($table)
        {
            $table->decimal('valor_hora_avulsa_padrao', 10, 2)->nullable();
            $table->decimal('valor_perdia_padrao', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_tipo_quarto', function($table)
        {
            $table->dropColumn('valor_hora_avulsa_padrao');
            $table->dropColumn('valor_perdia_padrao');
        });
    }
}
