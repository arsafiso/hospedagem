<?php namespace october\mot\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateOctoberMotReserva6 extends Migration
{
    public function up()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->string('placa', 10)->nullable();
            $table->string('veiculo', 255)->nullable();
            $table->integer('qtd_pessoas')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('october_mot_reserva', function($table)
        {
            $table->dropColumn('placa');
            $table->dropColumn('veiculo');
            $table->dropColumn('qtd_pessoas');
        });
    }
}
