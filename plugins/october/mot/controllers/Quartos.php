<?php namespace october\mot\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Quartos extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'mot_quarto' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.mot', 'main-menu-item', 'side-menu-item');
    }

    public function listExtendQuery($query)
    {
        //$query->Where('status', 1);
    }
}
