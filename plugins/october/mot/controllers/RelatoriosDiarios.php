<?php namespace october\mot\Controllers;

use Db;
use BackendMenu;
use October\Mot\Models\Reserva;
use Backend\Classes\Controller;
use October\Rain\Exception\ApplicationException;
use Renatio\DynamicPDF\Classes\PDF; // import facade
use October\Glo\Models\Parametro;
use October\Cfe\Models\HistoricoFechamento;

class RelatoriosDiarios extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'mot_reserva' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.mot', 'main-menu-item2', 'side-menu-item');
        $this->onStart();

    }

    public function realizaParcial(){
        trace_log('realizaParcial');
        //$abertura = //aqui preciso passar a última abertura salva no histórico
        $historico = HistoricoFechamento::OrderByDesc('id')->first();
        if($historico) {
            trace_log($historico->created_at);
            $abertura = $historico->created_at;
            trace_log('created');
        }else{
            $abertura = '2000-01-01 00:00:00';
        }
        $this->listaEstoque($abertura);
    }

    public function salvaHistorico($total, $definitivo){
        $historico = new HistoricoFechamento();
        $historico->valor = $total;
        $historico->fechamento_definitivo = ($definitivo ? 1 : 2);
        //$historico->created_at = date('Y-m-d H:i:s');
        $historico->save();
    }

    public function listaEstoque($abertura = false, $definitivo = false)
    {
        if(!$abertura){
            $abertura = date('Y-m-d').' 07:00:00';
            $fechamento = date('Y-m-d').' 19:00:00';
        }else{
            $fechamento = date('Y-m-d H:i:s');
        }

        $query = Db::select("SELECT res.id, quarto.numero, res.entrada, res.valor_estadia, res.valor_produtos, res.valor_desconto, res.valor, forma_pgto.nome as forma_pgto FROM october_mot_reserva AS res
                INNER JOIN october_mot_quarto AS quarto ON res.quarto_id = quarto.id
                LEFT JOIN october_cfe_movimento AS mov ON mov.tabela_origem = 'mot_reserva' AND mov.tabela_origem_id = res.id
                LEFT JOIN october_cfe_item_movimento AS item ON mov.id = item.movimento_id
                LEFT JOIN october_fin_lancamento AS fin ON fin.tabela_origem = 'mot_reserva' AND fin.tabela_origem_id = res.id
                LEFT JOIN october_fin_forma_pgto AS forma_pgto ON forma_pgto.id = fin.forma_pgto_id
                GROUP BY res.id");
        
        $query_total = Db::select("SELECT COUNT(res.id) AS quantidade, SUM(res.valor_estadia) AS hospedagem, SUM(res.valor_produtos) AS pedidos, SUM(res.valor_desconto) AS desconto, SUM(res.valor) AS recebido 
                        FROM october_mot_reserva AS res");

        $query_quartos = Db::select("SELECT COUNT(res.id) AS quantidade, tipo.nome, SUM(res.valor_estadia) AS hospedagem, SUM(res.valor_produtos) AS pedidos, SUM(res.valor_desconto) AS desconto, SUM(res.valor) AS recebido
                        FROM october_mot_reserva AS res
                        INNER JOIN october_mot_quarto AS quarto ON res.quarto_id = quarto.id
                        INNER JOIN october_mot_tipo_quarto AS tipo ON tipo.id = quarto.tipo_quarto_id
                        GROUP BY tipo.id");

        $query_entrada = Db::select("SELECT COUNT(res.id) AS entrada FROM october_mot_reserva AS res WHERE entrada >= '".$abertura."'");

        $query_anterior = Db::select("SELECT COUNT(res.id) AS anterior FROM october_mot_reserva AS res WHERE saida IS NULL AND entrada < '".$abertura."'");

        $query_pagos = Db::select("SELECT COUNT(res.id) AS pagos FROM october_mot_reserva AS res WHERE saida >= '".$abertura."' AND saida <= '".$fechamento."' AND entrada < '".$abertura."'");

        $query_pagos_quartos = Db::select("SELECT COUNT(res.id) AS quantidade, tipo.nome FROM october_mot_reserva AS res
                        INNER JOIN october_mot_quarto AS quarto ON res.quarto_id = quarto.id
                        INNER JOIN october_mot_tipo_quarto AS tipo ON tipo.id = quarto.tipo_quarto_id
                        WHERE saida >= '".$abertura."' AND saida <= '".$fechamento."' AND entrada < '".$abertura."'
                        GROUP BY tipo.id");

        $parametros = ['tipo_mov_nota_fiscal', 'tipo_mov_financeiro'];
        $param = Parametro::whereIn('nome', $parametros)->lists('valor');
        $params = implode(',', $param);        
        $query_movimentos = Db::select("SELECT tipo_mov.nome, SUM(valor) as valor FROM october_cfe_movimento AS mov
                        INNER JOIN october_cfe_tipo_movimento AS tipo_mov ON tipo_mov.id = mov.tipo_mov_id
                        WHERE tipo_mov_id IN (".$params.")
                        GROUP BY tipo_mov_id");

        $total = $query_total[0]->recebido;
        $this->salvaHistorico($total, $definitivo);

        $this->vars['reserva'] = $query;
        $this->vars['reserva_total'] = $query_total;
        $this->vars['reserva_quartos'] = $query_quartos;
        $this->vars['data'] = date('d/m/Y');
        $this->vars['hora'] = date('H:i:s');
        $this->vars['entrada'] = $query_entrada[0]->entrada;
        $this->vars['anterior'] = $query_anterior[0]->anterior;
        $this->vars['passante'] = '';
        $this->vars['pagos'] = $query_pagos[0]->pagos;
        $this->vars['pagos_quartos'] = $query_pagos_quartos;
        $this->vars['movimentos'] = $query_movimentos;
        return $this->makePartial('lista_estoque');
    }

    public function onStart()
    {
//        return PDF::loadTemplate('relatorio-diario')->stream();
    }

    public function pdf()
    {   
        trace_log('entrou');
        /* $data = ['name' => 'John Doe']; // optional data used in template
        $templateCode = 'relatorio-diario'; // unique code of the template */
//        return PDF::loadTemplate($templateCode, $data)->download('download.pdf');

        /* return PDF::loadTemplate('relatorio-diario')
            ->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif'])
            ->stream();
 */
        $this->pageTitle = trans('renatio.dynamicpdf::lang.templates.preview_pdf');
        $id = 1;
        try {
            $model = $this->formFindModelObject($id);
        } catch (ApplicationException $e) {
            return $this->handleError($e);
        }

        return PDF::loadTemplate($model->code)
            ->setOptions([
                'logOutputFile' => storage_path('temp/log.htm'),
                'isRemoteEnabled' => true,
            ])
            ->stream();

        /* $pdf = PDF::loadView('pdf.invoice', $data);
        return $pdf->download('invoice.pdf'); */
    }
}
