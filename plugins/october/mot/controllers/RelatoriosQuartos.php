<?php namespace october\mot\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class RelatoriosQuartos extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'mot_reserva' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.mot', 'main-menu-item2', 'side-menu-item2');
    }
}
