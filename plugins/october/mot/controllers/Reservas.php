<?php namespace october\mot\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Db;
use Request;
use October\Mot\Models\Reserva;
use October\Mot\Models\Quarto;

class Reservas extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'mot_reserva' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('october.mot', 'main-menu-item', 'side-menu-item3');
    }

    public function index()
    {
        $this->vars['total'] = $this->calculaValorParcial();
        $this->vars['quartos'] = $this->calculaQuartoParcial();
        return $this->asExtension('ListController')->index();
    }

    public function calculaValorParcial(){
        
        $reservas = Reserva::whereNotNull('entrada')->whereNull('saida')->get();
        
        if($reservas){
            $total = 0;
            foreach($reservas as $reserva){
                $total += $reserva->calculaValor();
            }
        }

        return $total;
    }

    public function calculaQuartoParcial(){
        return Reserva::whereNotNull('entrada')->whereNull('saida')->get()->count();
    }

    public function listExtendQuery($query)
    {
        $query->WhereNull('saida');
    }

    public function formExtendModel($model)
    {
        /* $model->valor_estadia_original = $model->id; */
    }

    public function dashboard()
    {
        $this->addCss('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css');
        $this->addCss('https://use.fontawesome.com/releases/v5.7.2/css/all.css');
        $this->addCss('netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css');

        $this->vars['today'] = date('d/m/Y');
        $reserva = Db::select("SELECT DISTINCT `october_mot_quarto`.`numero`, `october_mot_quarto`.`status`, october_mot_reserva.id, entrada FROM `october_mot_quarto` 
        LEFT JOIN `october_mot_reserva` ON `october_mot_reserva`.`quarto_id` = `october_mot_quarto`.`id` AND october_mot_reserva.id IN (SELECT MAX(id) FROM october_mot_reserva WHERE saida IS NULL AND quarto_id = october_mot_quarto.id)
        WHERE `october_mot_quarto`.`deleted_at` IS NULL
        ORDER BY numero");
        
        $this->vars['reserva'] = $reserva;
        return $this->makePartial('dashboard');
    }
    
    public function atualizaReserva($post)
    {
        if(!isset($post) || empty($post)){
            return;
        }
        
        $id = Request::segment(6);
        //$segment1 = Request::segment(7);
        //$segment2 = Request::segment(8);
        
        $quarto = Quarto::where('numero', $post['quarto'])->first();
        $quarto_id = $quarto->id;

        $reserva = Reserva::where('id', $id)->first();
        if($reserva){
            $reserva->saida = date('Y-m-d H:i:s');
            $reserva->updated_at = date('Y-m-d H:i:s');
            $reserva->save();
            $quarto_status = 1;
        } else {
            $reserva = new Reserva;
            $reserva->quarto_id = $quarto_id;
            $reserva->entrada = date('Y-m-d H:i:s');
            $reserva->created_at = date('Y-m-d H:i:s');
            $reserva->save();
            $quarto_status = 0;
        }
        
        if($reserva){
            $quarto->status = $quarto_status;
            $quarto->save();
        }

        return $reserva;
        //echo "<script> alert('entrou'); window.location.assign('http://localhost/erp.4tech.mobi/backend/october/mot/reservashistorico/dashboard'); </script>";
        //echo "<script> window.location.assign('http://localhost/erp.4tech.mobi/backend/october/mot/reservashistorico/dashboard'); </script>";
    }
}